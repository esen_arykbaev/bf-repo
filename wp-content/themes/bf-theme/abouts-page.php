<?php
/**
 * Template Name: Шаблон страницы о заботах
 */

get_header(); ?>

<main id="main" class="site-main" role="main">

<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php the_title(); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

            <div class="banner-menu">
            	<?php 
					$link = get_field('ssylka_1');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_2');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_3');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
            </div>

        </div>
    </div>
</section>


<section class="company">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 "> <!--col-xl-8-->
                <div class="credits-scroll">
                    <ul class="nav nav-tabs credits-tabs" id="myTab" role="tablist">
                    	
                    	<?php if( get_field('nasha_missiya') ) { ?> 
	                		<li class="nav-item" role="presentation">
	                            <a href="#client" class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tab-content-1" type="button" role="tab" aria-controls="tab-content-1" aria-selected="true">
	                                Забота о клиентах
	                            </a>
	                        </li>
	                	<?php } ?> 
	                	
                        <?php if( get_field('nasha_kompaniya') ) { ?> 
	                        <li class="nav-item" role="presentation">
	                            <a href="#sotr" class="nav-link" id="tab-2" data-bs-toggle="tab" data-bs-target="#tab-content-2" type="button" role="tab" aria-controls="tab-content-2" aria-selected="false">
	                                Забота о сотрудниках
	                            </a>
	                        </li>
                        <?php } ?> 
                        
                        <?php if( get_field('nasha_proekty') ) { ?> 
	                        <li class="nav-item" role="presentation">
	                            <a href="#history" class="nav-link" id="tab-3" data-bs-toggle="tab" data-bs-target="#tab-content-3" type="button" role="tab" aria-controls="tab-content-3" aria-selected="false">
	                                История успеха
	                            </a>
	                        </li>
                        <?php } ?> 
                        
                        <?php if( get_field('blagotvoritelnos') ) { ?> 
	                        <li class="nav-item" role="presentation">
	                            <a href="#blago" class="nav-link" id="tab-4" data-bs-toggle="tab" data-bs-target="#tab-content-4" type="button" role="tab" aria-controls="tab-content-4" aria-selected="false">
	                                Благотворительность
	                            </a>
	                        </li>
                        <?php } ?> 
                        
                    </ul>
                </div>
                <div class="tab-content credits-tabs-content" id="myTabContent">
                	<?php if( get_field('nasha_missiya') ) { ?> 
	                    <div class="tab-pane fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">
	                        <?php the_field('nasha_missiya'); ?> 
	                        <div class="row mb-5">
			                	<?php $params = array('posts_per_page' => 9, 'cat' => array(5) ); 
			            	    if (have_posts()) : query_posts($params); ?>
								<?php while (have_posts()) : the_post(); ?>
			                    <div class="col-12 col-sm-6 col-md-4">
			                        <a href="<?php the_permalink(); ?>" class="news-block">
			                        	<?php if ( function_exists( 'add_theme_support' ) )
											the_post_thumbnail( array(370,9999), array('class' => 'news-block__img') ); 
										?>
			                            <div class="news-block__date"><?php echo get_the_date('d.m.Y'); ?></div>
			                            <p class="news-block__text"><?php the_title(); ?></p>
			                        </a>
			                    </div>
			                    <?php endwhile; ?>
					               <div class="row text-center d-none" >
									   <div class="col-xs-12">
									  	<?php the_posts_pagination( array (
													'mid_size' => 5,
													'prev_next'    => true, 
													'prev_text'    => __('«'),
													'next_text'    => __('»'),
													'type'         => 'list',
												)); ?>
									   </div>
									</div>
				    			<?php else : ?>
								<?php endif; wp_reset_query(); ?>
			                </div>
	                    </div>
                    <?php } ?>
                    
                    <?php if( get_field('nasha_kompaniya') ) { ?> 
	                    <div class="tab-pane fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
	                        <?php the_field('nasha_kompaniya'); ?>
	                        <div class="row mb-5">
			                	<?php $params = array('posts_per_page' => 9, 'cat' => array(6) ); 
			            	    if (have_posts()) : query_posts($params); ?>
								<?php while (have_posts()) : the_post(); ?>
			                    <div class="col-12 col-sm-6 col-md-4">
			                        <a href="<?php the_permalink(); ?>" class="news-block">
			                        	<?php if ( function_exists( 'add_theme_support' ) )
											the_post_thumbnail( array(370,9999), array('class' => 'news-block__img') ); 
										?>
			                            <div class="news-block__date"><?php echo get_the_date('d.m.Y'); ?></div>
			                            <p class="news-block__text"><?php the_title(); ?></p>
			                        </a>
			                    </div>
			                    <?php endwhile; ?>
					               <div class="row text-center d-none" >
									   <div class="col-xs-12">
									  	<?php the_posts_pagination( array (
													'mid_size' => 5,
													'prev_next'    => true, 
													'prev_text'    => __('«'),
													'next_text'    => __('»'),
													'type'         => 'list',
												)); ?>
									   </div>
									</div>
				    			<?php else : ?>
								<?php endif; wp_reset_query(); ?>
			                </div>
	                    </div>
                    <?php } ?>
                    
                    <?php if( get_field('nasha_proekty') ) { ?> 
	                    <div class="tab-pane fade" id="tab-content-3" role="tabpanel" aria-labelledby="tab-3">
	                        <?php the_field('nasha_proekty'); ?>
	                        
	                        <div class="row mb-5">
			                	<?php $params = array('posts_per_page' => 9, 'cat' => array(8) ); 
			            	    if (have_posts()) : query_posts($params); ?>
								<?php while (have_posts()) : the_post(); ?>
			                    <div class="col-12 col-sm-6 col-md-4">
			                        <a href="<?php the_permalink(); ?>" class="news-block">
			                        	<?php if ( function_exists( 'add_theme_support' ) )
											the_post_thumbnail( array(370,9999), array('class' => 'news-block__img') ); 
										?>
			                            <div class="news-block__date"><?php echo get_the_date('d.m.Y'); ?></div>
			                            <p class="news-block__text"><?php the_title(); ?></p>
			                        </a>
			                    </div>
			                    <?php endwhile; ?>
					               <div class="row text-center d-none" >
									   <div class="col-xs-12">
									  	<?php the_posts_pagination( array (
													'mid_size' => 5,
													'prev_next'    => true, 
													'prev_text'    => __('«'),
													'next_text'    => __('»'),
													'type'         => 'list',
												)); ?>
									   </div>
									</div>
				    			<?php else : ?>
								<?php endif; wp_reset_query(); ?>
			                </div>
	                    </div>
                    <?php } ?>
                    
                    <?php if( get_field('blagotvoritelnos') ) { ?> 
	                    <div class="tab-pane fade" id="tab-content-4" role="tabpanel" aria-labelledby="tab-4">
	                        <?php the_field('blagotvoritelnos'); ?>
	                        
	                        <div class="row mb-5">
			                	<?php $params = array('posts_per_page' => 9, 'cat' => array(7) ); 
			            	    if (have_posts()) : query_posts($params); ?>
								<?php while (have_posts()) : the_post(); ?>
			                    <div class="col-12 col-sm-6 col-md-4">
			                        <a href="<?php the_permalink(); ?>" class="news-block">
			                        	<?php if ( function_exists( 'add_theme_support' ) )
											the_post_thumbnail( array(370,9999), array('class' => 'news-block__img') ); 
										?>
			                            <div class="news-block__date"><?php echo get_the_date('d.m.Y'); ?></div>
			                            <p class="news-block__text"><?php the_title(); ?></p>
			                        </a>
			                    </div>
			                    <?php endwhile; ?>
					               <div class="row text-center d-none" >
									   <div class="col-xs-12">
									  	<?php the_posts_pagination( array (
													'mid_size' => 5,
													'prev_next'    => true, 
													'prev_text'    => __('«'),
													'next_text'    => __('»'),
													'type'         => 'list',
												)); ?>
									   </div>
									</div>
				    			<?php else : ?>
								<?php endif; wp_reset_query(); ?>
			                </div>
	                    </div>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>

</section>



<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<h2>Partners</h2>
    	<?php } else { ?>
    		<h2>Наши партнеры</h2>
    	<?php } ?>
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>

</main><!-- .site-main -->

<script>
	document.querySelectorAll('.credits-tabs a.nav-link').forEach(link => {
	    link.onclick = () => window.history.pushState(null, null, link.attributes.href.value);
	});
</script>

<?php get_footer(); ?>
