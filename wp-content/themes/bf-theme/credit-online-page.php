<?php
/**
 * Template Name: Шаблон страницы калькулятора
 */

get_header(); ?>

<main id="main" class="site-main" role="main">

<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php the_title(); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

            <div class="banner-menu">
            	<?php 
					$link = get_field('ssylka_1');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_2');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_3');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
            </div>

        </div>
    </div>
</section>

<section class="payments p-0">
	<div class=" my-5">
      <div class="container credit-container" id="credit-container">

	        <div class="scroll-block">
	            <ul class="nav nav-tabs online-tabs" id="myTab" role="tablist">
	                <li class="nav-item" role="presentation">
	                    <a href="#credit" class="nav-link " id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">
		                    <span class="d-flex align-items-center">
		                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/calc.svg" alt="">
		                        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Calculate loan<br/> in cash
						    	<?php } else { ?>
						    		Рассчитать кредит <br/> наличными
						    	<?php } ?>
		                        
		                    </span>
	                    </a>
	                </li>
	                <li class="nav-item" role="presentation">
	                    <a href="#online" class="nav-link active" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">
		                    <span class="d-flex align-items-center">
		                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/card.svg" alt="">
		                        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Apply online <br/> credit
						    	<?php } else { ?>
						    		Оформи онлайн <br/> кредит
						    	<?php } ?>
		                        
		                    </span>
	                    </a>
	                </li>
	                <li class="nav-item" role="presentation">
	                    <a href="#partner" class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">
		                    <span class="d-flex align-items-center">
		                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/cart.svg" alt="">
		                        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Become our partner <br/> supplier
						    	<?php } else { ?>
						    		Стань нашим партнером <br/> поставщиком
						    	<?php } ?>
		                        
		                    </span>
	                    </a>
	                </li>
	            </ul>
	        </div>
	
	        <div class="tab-content" id="myTabContent">
	            <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
	
	                <div class="row p-4">
	                    <div class="col-sm-12 col-md-12 col-xxl-7 mb-3">
	
	                        <div class="credit-container__left">
	                            
								<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	<h5>Credit <span class="text__red-dark">calculator</span></h5>
						    	<?php } else { ?>
						    		<h5>Кредитный <span class="text__red-dark">калькулятор</span></h5>
						    	<?php } ?>
	                            <div class="row mb-3">
	                                <div class="col-sm-12 col-md-4">
	                                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
									    	<label for="selectCredit" class="d-block">Loan type</label>
											<select name="" id="selectCredit">
												<option value="30">Health</option>
												<option value="35">Home Improvement</option>
												<option value="38">Agro credit</option>
												<option value="28">Business loan</option>
												<option value="25">Murabaha</option>
												<option value="45">Min Turkun</option>
												<option value="22">RES Credit</option>
												<option value="32">VAC Credit</option>
												<option value="28">Consumer</option>
											</select>
								    	<?php } else { ?>
								    		<label for="selectCredit" class="d-block">Тип кредита</label>
		                                    <select name="" id="selectCredit">
		                                        <option value="30">Здоровье</option>
		                                        <option value="35">Улучшение жилья</option>
		                                        <option value="38">Агро кредит</option>
		                                        <option value="28">Бизнес кредит</option>
		                                        <option value="25">Мурабаха</option>
		                                        <option value="45">Миң түркүн</option>
		                                        <option value="22">ВИЭ Кредит</option>
		                                        <option value="32">ЦДС Кредит</option>
		                                        <option value="28">Потребительский</option>
		                                    </select>
								    	<?php } ?>
	                                    
	                                </div>
	                                <div class="col-sm-12 col-md-8">
	                                    <div class="ty-control-group slider_universal-block">
	                                        <div class="d-flex align-items-center justify-content-between mb-3">
	                                        	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
											    	<label for="slider_universal" class="m-0">Select loan amount</label>
													<p class="slider_universal_texts_payment">
														<input type="number" min="5000" max="50000" id="universal_hidden" name="calc_slider" value="50000" />
														<span>s</span>
													</p>
										    	<?php } else { ?>
										    		<label for="slider_universal" class="m-0">Выберите размер кредита</label>
		                                            <p class="slider_universal_texts_payment">
		                                                <input type="number" min="5000" max="50000" id="universal_hidden" name="calc_slider" value="50000" />
		                                                <span>c</span>
		                                            </p>
										    	<?php } ?>
	                                            
	                                        </div>
	                                        <div id="slider_universal" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"></div>
	                                        <div class="slider_universal_terms">
	                                            <div id="slider_universal_min_term">5000</div>
	                                            <div id="slider_universal-result">25000</div>
	                                            <div id="slider_universal_max_term">50000</div>
	                                        </div>
	                                    </div>
	
	                                </div>
	                            </div>
	
								<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
									  <div class="row">
		                                <div class="col-sm-12 col-md-4">
		                                    <label class="d-block">Interest rate %</label>
		                                    <input type="number" min="1" max="100" id="percentage-block" value="30" class="percentage-block">
		                                </div>
		                                <div class="col-sm-12 col-md-8">
		                                    <div class="ty-control-group slider_universal-block-2">
		                                        <div class="d-flex align-items-center justify-content-between mb-3">
		                                            <p>
		                                                <label for="slider_universal_min_payment-hidden" class="m-0">Select term (mon)</label>
		                                            </p>
		                                            <p class="slider_universal_texts_payment slider_day">
		                                                <input type="number" min="1" max="18" id="slider_universal_min_payment-hidden" name="calc_slider2" value="12">
		                                            </p>
		                                        </div>
		
		                                        <div id="slider_universal_min_payment" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"></div>
		                                        <div class="slider_universal_first_p">
		                                            <div id="slider_universal_min_first_count">1 month</div>
													<div id="slider_universal_min_count-result">12 months</div>
													<div id="slider_universal_max_first_count">18 months</div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>	
						    	<?php } else { ?>
						    		<div class="row">
		                                <div class="col-sm-12 col-md-4">
		                                    <label class="d-block">Процентная ставка %</label>
		                                    <input type="number" min="1" max="100" id="percentage-block" value="30" class="percentage-block">
		                                </div>
		                                <div class="col-sm-12 col-md-8">
		                                    <div class="ty-control-group slider_universal-block-2">
		                                        <div class="d-flex align-items-center justify-content-between mb-3">
		                                            <p>
		                                                <label for="slider_universal_min_payment-hidden" class="m-0">Выберите срок (мес)</label>
		                                            </p>
		                                            <p class="slider_universal_texts_payment slider_day">
		                                                <input type="number" min="1" max="18" id="slider_universal_min_payment-hidden" name="calc_slider2" value="12">
		                                            </p>
		                                        </div>
		
		                                        <div id="slider_universal_min_payment" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"></div>
		                                        <div class="slider_universal_first_p">
		                                            <div id="slider_universal_min_first_count">1 мес</div>
		                                            <div id="slider_universal_min_count-result">12 мес</div>
		                                            <div id="slider_universal_max_first_count">18 мес</div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
						    	<?php } ?>
										    	
										    	
	                            
	                        </div>
	
	                    </div>
	                    <div class="col-sm-12 col-md-12 col-xxl-5 mb-3">
	                        <div class="credit-container__right">
	                            <div class="row">
	                                <div class="col-ms-12 col-md-6">
	                                	
	                                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
											<h5 class="p-0 text-center">Our <span class="text__red-dark">offer</span></h5>
											<div class="calc_result">
												<div class="result_item">
													<p>Monthly payment</p>
													<p id="price">4874</p>
												</div>
												<a href="#1"
												class="button-secondary button-secondary__outline w-100 calc_result--btn"
												data-bs-toggle="modal"
												data-bs-target="#calcModal"
												data-credit="58492"
												data-month="12"
												data percentage="30">
												Submit 
												</a>
												<p class="calc_result--info mt-4">
													An example of calculating the interest rate is for informational purposes only and is not a public offer
												</p>
											</div>
								    	<?php } else { ?>
								    		<h5 class="p-0 text-center">Наше <span class="text__red-dark">предложение</span></h5>
		                                    <div class="calc_result">
		                                        <div class="result_item">
		                                            <p>Eжемесячный платеж</p>
		                                            <p id="price">4874</p>
		                                        </div>
		                                        <a href="#1"
		                                           class="button-secondary button-secondary__outline w-100 calc_result--btn"
		                                           data-bs-toggle="modal"
		                                           data-bs-target="#calcModal"
		                                           data-credit="58492"
		                                           data-month="12"
		                                           data-procent="30">
		                                            Оставить заявку
		                                        </a>
		                                        <p class="calc_result--info mt-4">
		                                            Пример расчёта процентной ставки носит исключительно информационный характер и не является публичной офертой
		                                        </p>
		                                    </div>
								    	<?php } ?>
	                                    
	                                </div>
	                                <div class="col-ms-12 col-md-6">
	                                    <div class=" d-flex justify-content-center flex-column ps-md-3 h-100">
	                                    	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	                                    	<div class="result_item">
											<p>Interest rate</p>
											<p id="initpay" class="m-0">30%</p>
											</div>
											<div class="result_item">
											<p>Total loan</p>
											<p id="allSum" class="m-0">58492 </p>
											</div>
											<div class="result_item m-0">
											<p>Loan term</p>
											<p id="allDay" class="m-0">12</p>
											</div>
	                                    	<?php } else { ?>
		                                        <div class="result_item">
		                                            <p>Процентная ставка</p>
		                                            <p id="initpay" class="m-0">30 %</p>
		                                        </div>
		                                        <div class="result_item">
		                                            <p>Общая сумма кредита</p>
		                                            <p id="allSum" class="m-0">58492 </p>
		                                        </div>
		                                        <div class="result_item m-0">
		                                            <p>Срок кредита</p>
		                                            <p id="allDay" class="m-0">12</p>
		                                        </div>
	                                        <?php } ?>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	
	            </div>
	            <div class="tab-pane fade  show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
	                <div class="container">
	                    <div class="credit-online">
	                        <h5>Заполните форму</h5>
	                        <?php echo do_shortcode('[contact-form-7 id="965" title="Оформи онлайн кредит"]'); ?>
	                    </div>
	                </div>
	            </div>
	            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
	                <div class="container">
	                    <div class="credit-online">
	                        <h5>Заполните форму</h5>
	                        <?php echo do_shortcode('[contact-form-7 id="966" title="Стань нашим партнером & поставщиком"]'); ?>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    
    </div>
    <div class="container my-5">
      <?php the_content(); ?>
    </div>
</section>


<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<h2>Partners</h2>
    	<?php } else { ?>
    		<h2>Наши партнеры</h2>
    	<?php } ?>
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>

</main><!-- .site-main -->

<script>
	document.querySelectorAll('.scroll-block a.nav-link').forEach(link => {
	    link.onclick = () => window.history.pushState(null, null, link.attributes.href.value);
	});
</script>

<?php get_footer(); ?>
