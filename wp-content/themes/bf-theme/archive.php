<?php get_header(); ?>
<style>
	.news-archive__date {
		    flex: 0 0 104px;
	}
	
</style>
<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1><?php wp_title("", true); ?></h1>
	            <p>
	            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	            	Be aware of all events
	            	<?php } else { ?>
	            	Будь в курсе всех событий
	            	<?php } ?>
	            	</p>
            </div>

            <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
		    	<div class="banner-menu">
	            	<a class="d-flex align-items-center" href="/en/o-kompanii/">
				    	About us
				    </a>
				    <a class="d-flex align-items-center" href="/en/news/">
				    	News
				    </a>
				    <a class="d-flex align-items-center" href="/en/chavo/">
				    	FAQ
				    </a>
	            </div>
	    	<?php } else { ?>
	    		<div class="banner-menu">
	            	<a class="d-flex align-items-center" href="/o-kompanii/">
				    	О компании
				    </a>
				    <a class="d-flex align-items-center" href="/news/">
				    	Новости
				    </a>
				    <a class="d-flex align-items-center" href="/chavo/">
				    	ЧАВО
				    </a>
	            </div>
	    	<?php } ?>
            

        </div>
    </div>
</section>

<section class="news-section">
    <div class="container">

        <div class="row">
        	<div class="col-12">
        		<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
			    	<h3 class="news-archive__title">Archive news</h3>
		    	<?php } else { ?>
		    		<h3 class="news-archive__title">Архив новостей</h3>
		    	<?php } ?>
                        
                    </div>
            <div class="col-12 col-md-9 m-0 p-0">
                <div class="row mb-5">
                	<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
                    <div class="col-12">
                        <a href="<?php the_permalink(); ?>" class="news-block d-none">
                        	<?php if ( function_exists( 'add_theme_support' ) )
								the_post_thumbnail( array(370,9999), array('class' => 'news-block__img') ); 
							?>
                            <div class="news-block__date"><?php echo get_the_date('d.m.Y'); ?></div>
                            <p class="news-block__text"><?php the_title(); ?></p>
                        </a>
                        <div class="news-archive w-100">
                            <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
			            		<a href="/en/<?php echo get_the_date('Y'); ?>" class="news-archive__date text-decoration-none">
	                            	<?php echo get_the_date('d.m.Y'); ?>
	                            </a>
				        	<?php } else { ?>
				        		<a href="/<?php echo get_the_date('Y'); ?>" class="news-archive__date text-decoration-none">
	                            	<?php echo get_the_date('d.m.Y'); ?>
	                            </a>
				        	<?php } ?>
                            <a href="<?php the_permalink(); ?>" class="news-archive__info text-decoration-none">
                                <?php the_title(); ?>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
		               <div class="row text-center d-none" >
						   <div class="col-xs-12">
						  	<?php the_posts_pagination( array (
										'mid_size' => 5,
										'prev_next'    => true, 
										'prev_text'    => __('«'),
										'next_text'    => __('»'),
										'type'         => 'list',
									)); ?>
						   </div>
						</div>
	    			<?php else : ?>
					<?php endif; wp_reset_query(); ?>
                </div>
                
            </div>
            <div class="col-12 col-md-3">
            	<?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
					<?php dynamic_sidebar( 'sidebar-4' ); ?>
				<?php endif; ?>
				
				<section class="news-container company-news-container">
						<h3 class="text-center mb-3">Истории успеха</h3>
	                    <div class="splide" id="news-slider">
	                        <div class="splide__track">
	                            <ul class="splide__list">
	                            	<?php $params = array('posts_per_page' => 3, 'cat' => array(8) ); 
				            	    if (have_posts()) : query_posts($params); ?>
									<?php while (have_posts()) : the_post(); ?>
				                    <li class="splide__slide">
				                    	<a href="<?php the_permalink(); ?>" class="news-links text-decoration-none">
				                    		<?php if ( function_exists( 'add_theme_support' ) )
												the_post_thumbnail( array(526,9999), array('class' => 'img-fluid') ); 
											?>
		                                    <div class="news-info row">
		                                        <span class="d-inline-block text-truncate text-white" style="max-width: 348px;">
		                                           <?php the_title(); ?>
		                                        </span>
		                                        <p class="text-white mt-1 mb-0">
		                                            <?php echo get_the_date('d.m.Y'); ?>
		                                        </p>
		                                    </div>
				                    	</a>
				                    	
	                                </li>
				                    <?php endwhile; ?>
									<?php endif; wp_reset_query(); ?>
	                            </ul>
	                        </div>
	                        <div class="splide__arrows d-none">
	                            <button class="splide__arrow splide__arrow--prev">
	                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
	                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
	                                </svg>
	                            </button>
	                            <button class="splide__arrow splide__arrow--next">
	                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
	                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
	                                </svg>
	                            </button>
	                        </div>
	                    </div>
	                </section>
	                <a href="/stranicza-meropriyatii/#history" class="button-primary my-5" style="max-width: 288px;margin: 0 auto;">Все истории успеха</a>
				
            </div>
        </div>

    </div>
</section>
<?php get_footer(); ?>