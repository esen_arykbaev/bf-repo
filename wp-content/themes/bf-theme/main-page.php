<?php
/**
 * Template Name: Шаблон Главной страницы
 */
?>
<?php get_header(); ?>

<section class="home-carousel">
    <div id="main-slider" class="splide">
        <div class="splide__track">
            <ul class="splide__list">
            	<?php 
					$args = array('post_type' => 'maincarousel', 'posts_per_page' => 4  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
						<li class="splide__slide">
		                    <div class="container w-100">
		                        <div class="home-carousel__info">
		                            <h3 class="text__primary">
		                                <?php the_field('zagolovok'); ?>
		                            </h3>
		                            <div class="text__dark">
		                            	<?php the_field('opisanie'); ?>
		                            </div>
		
		                            <div class="d-flex align-items-center flex-wrap">
		                                <a href="<?php the_field('ssylka'); ?>" class="button-primary me-3">
		                                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
										    	Read more
									    	<?php } else { ?>
									    		Подробнее
									    	<?php } ?>
		                                </a>
		                                <button class="button-secondary" data-bs-toggle="modal" data-bs-target="#onlineModal">
		                                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
										    	Online ticket
									    	<?php } else { ?>
									    		Онлайн заявка
									    	<?php } ?>
		                                </button>
		                            </div>
		                        </div>
		                    </div>
		                    <img src="<?php the_field('banner'); ?>" alt="">
		                </li>
				<?php } wp_reset_postdata(); ?>
            </ul>
        </div>
    </div>

    <div class="thumbnails-scroll">
        <ul id="thumbnails" class="thumbnails">
        	<?php 
				$args = array('post_type' => 'maincarousel', 'posts_per_page' => 4  ); 
				$myposts = get_posts( $args );
				foreach( $myposts as $post ){ setup_postdata($post); ?>
				<li class="thumbnail">
	                <div class="thumbnail-info">
	                    <h5 class="text-uppercase mt-auto"><?php the_title(); ?></h5>
	                    <div><?php the_field('summa'); ?></div>
	                    <div><?php the_field('mesyacz'); ?></div>
						<a href="<?php the_field('ssylka'); ?>" class=" mt-2">
							<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
						    	Read more
					    	<?php } else { ?>
					    		Подробнее
					    	<?php } ?>
						</a>
	                    <div class="thumbnail-progress mt-auto">
	                        <div class="thumbnail-progress-bar"></div>
	                    </div>
	                </div>
	            </li>
			<?php } wp_reset_postdata(); ?>
        </ul>
    </div>
</section>

<section class="most-important">
    <div class="petal-icon d-none"></div>
    <div class="container">
        <h2>
        	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
		    	Most important
	    	<?php } else { ?>
	    		Самое важное
	    	<?php } ?>
        </h2>
        <div class="row">
            <div class="col-sm-12 col-md-6 col-xxl-4 mb-4">
                <section class="news-container">
                    <div class="splide" id="news-slider">
                        <div class="splide__track">
                            <ul class="splide__list">
                            	
                            	<?php $params = array('posts_per_page' => 3, 'cat' => array(1) ); 
			            	    if (have_posts()) : query_posts($params); ?>
								<?php while (have_posts()) : the_post(); ?>
			                    <li class="splide__slide">
			                    	<a href="<?php the_permalink(); ?>" class="news-links text-decoration-none">
			                    		<?php if ( function_exists( 'add_theme_support' ) )
											the_post_thumbnail( array(526,9999), array('class' => 'img-fluid') ); 
										?>
	                                    <div class="news-info row">
	                                        <span class="d-inline-block text-truncate text-white" style="max-width: 348px;">
	                                           <?php the_title(); ?>
	                                        </span>
	                                        <p class="text-white mt-1 mb-0">
	                                            <?php echo get_the_date('d.m.Y'); ?>
	                                        </p>
	                                    </div>
			                    	</a>
                                </li>
			                    <?php endwhile; ?>
								<?php endif; wp_reset_query(); ?>
                                
                            </ul>
                        </div>
                        <div class="splide__arrows">
                            <button class="splide__arrow splide__arrow--prev">
                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
                                </svg>
                            </button>
                            <button class="splide__arrow splide__arrow--next">
                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-sm-12 col-md-6 col-xxl-5 mb-4">
                <section class="news-container__list">
                    <h5>
                    	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
					    	News
				    	<?php } else { ?>
				    		Последние новости
				    	<?php } ?>
                    </h5>
                    <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
				    	<a href="/en/2021" style="position: absolute; right: 15px; top: 25px;font-size: 13px;">
                    		Archive news
                    	</a>
			    	<?php } else { ?>
			    		<a href="/2021" style="position: absolute; right: 15px; top: 25px;font-size: 13px;">
                    		Архив новостей
                    	</a>
			    	<?php } ?>
                    
                    <?php $params = array('posts_per_page' => 3, 'cat' => array(1) ); 
            	    if (have_posts()) : query_posts($params); ?>
					<?php while (have_posts()) : the_post(); ?>
					<div class="d-flex align-items-center mb-2">
                        <div class="news-container__date d-flex align-items-center justify-content-center flex-column">
                            <span class="news-container__date-count"><?php echo get_the_date('d'); ?></span>
                            <span class="news-container__date-month text__red"><?php echo get_the_date('M'); ?></span>
                        </div>
                        <div class="news-container__info position-relative w-100">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            <a href="<?php the_permalink(); ?>" class="text__red-dark text-end">
                            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	See...
						    	<?php } else { ?>
						    		Посмотреть...
						    	<?php } ?>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
					<?php endif; wp_reset_query(); ?>
                </section>
            </div>

            <div class="col-sm-12 col-md-6 col-xxl-3 mb-4">
                <aside class="exchange">
                    <div class="exchange-header d-flex align-items-center justify-content-between mb-4">
                    	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
					    	<span><span class="text__red-dark fw-bold">Exchange</span></span>
				    	<?php } else { ?>
				    		<span>Курс <span class="text__red-dark fw-bold">валют</span></span>
				    	<?php } ?>
                        
                        <span class="exchange-date">30.11.2021</span>
                    </div>
                    <table class="table table-bordered">
                        <tbody class="text-center">
                            <tr>
                                <td class="fw-bold text__red-dark"><?php the_field('valyuta') ?></td>
                                <td class="fw-bold text__red-dark"><?php the_field('pokupka') ?></td>
                                <td class="fw-bold text__red-dark"><?php the_field('prodazha') ?></td>
                            </tr>
                            <tr>
                                <td><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/united-states.svg" alt="" ></td>
                                <td><?php the_field('usd') ?></td>
                                <td><?php the_field('usd_2') ?></td>
                            </tr>
                            <tr>
                                <td><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/european-union.svg" alt="" ></td>
                                <td><?php the_field('euro') ?></td>
                                <td><?php the_field('euro_2') ?></td>
                            </tr>
                            <tr>
                                <td> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/russia.svg" alt=""></td>
                                <td><?php the_field('rub') ?></td>
                                <td><?php the_field('rub_2') ?></td>
                            </tr>
                            <tr>
                                <td><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/kazakhstan.svg" alt=""></td>
                                <td><?php the_field('kzt') ?></td>
                                <td><?php the_field('kzt_2') ?></td>
                            </tr>
                        </tbody>
                    </table>
                </aside>
            </div>

        </div>
    </div>

    <div class="container credit-container" id="credit-container">

        <div class="scroll-block">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
	                <a href="#credit" class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">
	                    <span class="d-flex align-items-center">
	                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/calc.svg" alt="">
	                        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
						    	Calculate loan<br/> in cash
					    	<?php } else { ?>
					    		Рассчитать кредит <br/> наличными
					    	<?php } ?>
	                        
	                    </span>
	                </a>
	            </li>
	            <li class="nav-item" role="presentation">
	                <a href="#online" class="nav-link " id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">
	                    <span class="d-flex align-items-center">
	                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/card.svg" alt="">
	                        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
						    	Apply online <br/> credit
					    	<?php } else { ?>
					    		Оформи онлайн <br/> кредит
					    	<?php } ?>
	                        
	                    </span>
	                </a>
	            </li>
	            <li class="nav-item" role="presentation">
	                <a href="#partner" class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">
	                    <span class="d-flex align-items-center">
	                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/cart.svg" alt="">
	                        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
						    	Become our partner <br/> supplier
					    	<?php } else { ?>
					    		Стань нашим партнером <br/> поставщиком
					    	<?php } ?>
	                        
	                    </span>
	                </a>
	            </li>
            </ul>
        </div>

        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
	
	                <div class="row p-4">
	                    <div class="col-sm-12 col-md-12 col-xxl-7 mb-3">
	
	                        <div class="credit-container__left">
	                            
								<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	<h5>Credit <span class="text__red-dark">calculator</span></h5>
						    	<?php } else { ?>
						    		<h5>Кредитный <span class="text__red-dark">калькулятор</span></h5>
						    	<?php } ?>
	                            <div class="row mb-3">
	                                <div class="col-sm-12 col-md-4">
	                                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
									    	<label for="selectCredit" class="d-block">Loan type</label>
											<select name="" id="selectCredit">
												<option value="30">Health</option>
												<option value="35">Home Improvement</option>
												<option value="38">Agro credit</option>
												<option value="28">Business loan</option>
												<option value="25">Murabaha</option>
												<option value="45">Min Turkun</option>
												<option value="22">RES Credit</option>
												<option value="32">VAC Credit</option>
												<option value="28">Consumer</option>
											</select>
								    	<?php } else { ?>
								    		<label for="selectCredit" class="d-block">Тип кредита</label>
		                                    <select name="" id="selectCredit">
		                                        <option value="30">Здоровье</option>
		                                        <option value="35">Улучшение жилья</option>
		                                        <option value="38">Агро кредит</option>
		                                        <option value="28">Бизнес кредит</option>
		                                        <option value="25">Мурабаха</option>
		                                        <option value="45">Миң түркүн</option>
		                                        <option value="22">ВИЭ Кредит</option>
		                                        <option value="32">ЦДС Кредит</option>
		                                        <option value="28">Потребительский</option>
		                                    </select>
								    	<?php } ?>
	                                    
	                                </div>
	                                <div class="col-sm-12 col-md-8">
	                                    <div class="ty-control-group slider_universal-block">
	                                        <div class="d-flex align-items-center justify-content-between mb-3">
	                                        	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
											    	<label for="slider_universal" class="m-0">Select loan amount</label>
													<p class="slider_universal_texts_payment">
														<input type="number" min="5000" max="50000" id="universal_hidden" name="calc_slider" value="50000" />
														<span>s</span>
													</p>
										    	<?php } else { ?>
										    		<label for="slider_universal" class="m-0">Выберите размер кредита</label>
		                                            <p class="slider_universal_texts_payment">
		                                                <input type="number" min="5000" max="50000" id="universal_hidden" name="calc_slider" value="50000" />
		                                                <span>c</span>
		                                            </p>
										    	<?php } ?>
	                                            
	                                        </div>
	                                        <div id="slider_universal" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"></div>
	                                        <div class="slider_universal_terms">
	                                            <div id="slider_universal_min_term">5000</div>
	                                            <div id="slider_universal-result">25000</div>
	                                            <div id="slider_universal_max_term">50000</div>
	                                        </div>
	                                    </div>
	
	                                </div>
	                            </div>
	
								<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
									  <div class="row">
		                                <div class="col-sm-12 col-md-4">
		                                    <label class="d-block">Interest rate %</label>
		                                    <input type="number" min="1" max="100" id="percentage-block" value="30" class="percentage-block">
		                                </div>
		                                <div class="col-sm-12 col-md-8">
		                                    <div class="ty-control-group slider_universal-block-2">
		                                        <div class="d-flex align-items-center justify-content-between mb-3">
		                                            <p>
		                                                <label for="slider_universal_min_payment-hidden" class="m-0">Select term (mon)</label>
		                                            </p>
		                                            <p class="slider_universal_texts_payment slider_day">
		                                                <input type="number" min="1" max="18" id="slider_universal_min_payment-hidden" name="calc_slider2" value="12">
		                                            </p>
		                                        </div>
		
		                                        <div id="slider_universal_min_payment" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"></div>
		                                        <div class="slider_universal_first_p">
		                                            <div id="slider_universal_min_first_count">1 month</div>
													<div id="slider_universal_min_count-result">12 months</div>
													<div id="slider_universal_max_first_count">18 months</div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>	
						    	<?php } else { ?>
						    		<div class="row">
		                                <div class="col-sm-12 col-md-4">
		                                    <label class="d-block">Процентная ставка %</label>
		                                    <input type="number" min="1" max="100" id="percentage-block" value="30" class="percentage-block">
		                                </div>
		                                <div class="col-sm-12 col-md-8">
		                                    <div class="ty-control-group slider_universal-block-2">
		                                        <div class="d-flex align-items-center justify-content-between mb-3">
		                                            <p>
		                                                <label for="slider_universal_min_payment-hidden" class="m-0">Выберите срок (мес)</label>
		                                            </p>
		                                            <p class="slider_universal_texts_payment slider_day">
		                                                <input type="number" min="1" max="18" id="slider_universal_min_payment-hidden" name="calc_slider2" value="12">
		                                            </p>
		                                        </div>
		
		                                        <div id="slider_universal_min_payment" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"></div>
		                                        <div class="slider_universal_first_p">
		                                            <div id="slider_universal_min_first_count">1 мес</div>
		                                            <div id="slider_universal_min_count-result">12 мес</div>
		                                            <div id="slider_universal_max_first_count">18 мес</div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
						    	<?php } ?>
										    	
										    	
	                            
	                        </div>
	
	                    </div>
	                    <div class="col-sm-12 col-md-12 col-xxl-5 mb-3">
	                        <div class="credit-container__right">
	                            <div class="row">
	                                <div class="col-ms-12 col-md-6">
	                                	
	                                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
											<h5 class="p-0 text-center">Our <span class="text__red-dark">offer</span></h5>
											<div class="calc_result">
												<div class="result_item">
													<p>Monthly payment</p>
													<p id="price">4874</p>
												</div>
												<a href="#1"
												class="button-secondary button-secondary__outline w-100 calc_result--btn"
												data-bs-toggle="modal"
												data-bs-target="#calcModal"
												data-credit="58492"
												data-month="12"
												data percentage="30">
												Submit 
												</a>
												<p class="calc_result--info mt-4">
													An example of calculating the interest rate is for informational purposes only and is not a public offer
												</p>
											</div>
								    	<?php } else { ?>
								    		<h5 class="p-0 text-center">Наше <span class="text__red-dark">предложение</span></h5>
		                                    <div class="calc_result">
		                                        <div class="result_item">
		                                            <p>Eжемесячный платеж</p>
		                                            <p id="price">4874</p>
		                                        </div>
		                                        <a href="#1"
		                                           class="button-secondary button-secondary__outline w-100 calc_result--btn"
		                                           data-bs-toggle="modal"
		                                           data-bs-target="#calcModal"
		                                           data-credit="58492"
		                                           data-month="12"
		                                           data-procent="30">
		                                            Оставить заявку
		                                        </a>
		                                        <p class="calc_result--info mt-4">
		                                            Пример расчёта процентной ставки носит исключительно информационный характер и не является публичной офертой
		                                        </p>
		                                    </div>
								    	<?php } ?>
	                                    
	                                </div>
	                                <div class="col-ms-12 col-md-6">
	                                    <div class=" d-flex justify-content-center flex-column ps-md-3 h-100">
	                                    	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	                                    	<div class="result_item">
											<p>Interest rate</p>
											<p id="initpay" class="m-0">30%</p>
											</div>
											<div class="result_item">
											<p>Total loan</p>
											<p id="allSum" class="m-0">58492 </p>
											</div>
											<div class="result_item m-0">
											<p>Loan term</p>
											<p id="allDay" class="m-0">12</p>
											</div>
	                                    	<?php } else { ?>
		                                        <div class="result_item">
		                                            <p>Процентная ставка</p>
		                                            <p id="initpay" class="m-0">30 %</p>
		                                        </div>
		                                        <div class="result_item">
		                                            <p>Общая сумма кредита</p>
		                                            <p id="allSum" class="m-0">58492 </p>
		                                        </div>
		                                        <div class="result_item m-0">
		                                            <p>Срок кредита</p>
		                                            <p id="allDay" class="m-0">12</p>
		                                        </div>
	                                        <?php } ?>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="container">
                    <div class="credit-online">
                        <h5>Заполните форму</h5>
						<?php echo do_shortcode('[contact-form-7 id="965" title="Оформи онлайн кредит"]'); ?>
                    </div>
                </div>
            </div>
            
            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <div class="container">
                    <div class="credit-online">
                        <h5>Заполните форму</h5>
                        <?php echo do_shortcode('[contact-form-7 id="966" title="Стань нашим партнером & поставщиком"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<h2>Partners</h2>
    	<?php } else { ?>
    		<h2>Наши партнеры</h2>
    	<?php } ?>
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>



<?php get_footer(); ?>