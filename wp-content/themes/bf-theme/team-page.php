<?php
/**
 * Template Name: Шаблон страницы сотрудников
 */

get_header(); ?>

<main id="main" class="site-main" role="main">

<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php the_title(); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

            <div class="banner-menu">
            	<?php 
					$link = get_field('ssylka_1');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_2');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_3');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
            </div>

        </div>
    </div>
</section>


<section class="team">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xl-8 director-left">
            	<?php if( have_rows('team') ): ?>
				    <?php while( have_rows('team') ): the_row(); ?>
					    <?php if( get_sub_field('imya') ) { ?>
						    <div class="card d-flex flex-row mb-5">
			                    <img src="<?php the_sub_field('kartinka'); ?>" class="img-fluid" alt="<?php the_sub_field('imya'); ?>">
			                    <div class="card-body">
			                        <h5 class="card-title"><?php the_sub_field('imya'); ?></h5>
			                        <p class="text__red-dark card-position"><?php the_sub_field('dolzhnost'); ?></p>
			                        <p class="card-text">
			                            <?php the_sub_field('opisanie'); ?>
			                        </p>
			                    </div>
			                </div>
					    <?php } ?>
				    <?php endwhile; ?>
			    <?php endif; ?>
			    <?php if( have_rows('team_2') ): ?>
				    <?php while( have_rows('team_2') ): the_row(); ?>
					    <?php if( get_sub_field('imya') ): ?>
						    <div class="card d-flex flex-row mb-5">
			                    <img src="<?php the_sub_field('kartinka'); ?>" class="img-fluid" alt="<?php the_sub_field('imya'); ?>">
			                    <div class="card-body">
			                        <h5 class="card-title"><?php the_sub_field('imya'); ?></h5>
			                        <p class="text__red-dark card-position"><?php the_sub_field('dolzhnost'); ?></p>
			                        <p class="card-text">
			                            <?php the_sub_field('opisanie'); ?>
			                        </p>
			                    </div>
			                </div>
					    <?php endif; ?>
				    <?php endwhile; ?>
			    <?php endif; ?>
			    <?php if( have_rows('team_3') ): ?>
				    <?php while( have_rows('team_3') ): the_row(); ?>
					    <?php if( get_sub_field('imya') ): ?>
						    <div class="card d-flex flex-row mb-5">
			                    <img src="<?php the_sub_field('kartinka'); ?>" class="img-fluid" alt="<?php the_sub_field('imya'); ?>">
			                    <div class="card-body">
			                        <h5 class="card-title"><?php the_sub_field('imya'); ?></h5>
			                        <p class="text__red-dark card-position"><?php the_sub_field('dolzhnost'); ?></p>
			                        <p class="card-text">
			                            <?php the_sub_field('opisanie'); ?>
			                        </p>
			                    </div>
			                </div>
					    <?php endif; ?>
				    <?php endwhile; ?>
			    <?php endif; ?>
			    <?php if( have_rows('team_4') ): ?>
				    <?php while( have_rows('team_4') ): the_row(); ?>
					    <?php if( get_sub_field('imya') ): ?>
						    <div class="card d-flex flex-row mb-5">
			                    <img src="<?php the_sub_field('kartinka'); ?>" class="img-fluid" alt="<?php the_sub_field('imya'); ?>">
			                    <div class="card-body">
			                        <h5 class="card-title"><?php the_sub_field('imya'); ?></h5>
			                        <p class="text__red-dark card-position"><?php the_sub_field('dolzhnost'); ?></p>
			                        <p class="card-text">
			                            <?php the_sub_field('opisanie'); ?>
			                        </p>
			                    </div>
			                </div>
					    <?php endif; ?>
				    <?php endwhile; ?>
			    <?php endif; ?>
			    <?php if( have_rows('team_5') ): ?>
				    <?php while( have_rows('team_5') ): the_row(); ?>
					    <?php if( get_sub_field('imya') ): ?>
						    <div class="card d-flex flex-row mb-5">
			                    <img src="<?php the_sub_field('kartinka'); ?>" class="img-fluid" alt="<?php the_sub_field('imya'); ?>">
			                    <div class="card-body">
			                        <h5 class="card-title"><?php the_sub_field('imya'); ?></h5>
			                        <p class="text__red-dark card-position"><?php the_sub_field('dolzhnost'); ?></p>
			                        <p class="card-text">
			                            <?php the_sub_field('opisanie'); ?>
			                        </p>
			                    </div>
			                </div>
					    <?php endif; ?>
				    <?php endwhile; ?>
			    <?php endif; ?>
			    <?php if( have_rows('team_6') ): ?>
				    <?php while( have_rows('team_6') ): the_row(); ?>
					    <?php if( get_sub_field('imya') ): ?>
						    <div class="card d-flex flex-row mb-5">
			                    <img src="<?php the_sub_field('kartinka'); ?>" class="img-fluid" alt="<?php the_sub_field('imya'); ?>">
			                    <div class="card-body">
			                        <h5 class="card-title"><?php the_sub_field('imya'); ?></h5>
			                        <p class="text__red-dark card-position"><?php the_sub_field('dolzhnost'); ?></p>
			                        <p class="card-text">
			                            <?php the_sub_field('opisanie'); ?>
			                        </p>
			                    </div>
			                </div>
					    <?php endif; ?>
				    <?php endwhile; ?>
			    <?php endif; ?>
            </div>

            <div class="col-sm-12 col-xl-4">
            	<style>
            		.ms-slide .ms-slide-bgcont img {
						border-radius: 10px;
						}
            	</style>
            	<div class="mt-5 px-lg-4">
            		<section class="news-container company-news-container">
	                    <div class="splide" id="news-slider">
	                        <div class="splide__track">
	                            <ul class="splide__list">
	                            	<?php $params = array('posts_per_page' => 3, 'cat' => array(1) ); 
				            	    if (have_posts()) : query_posts($params); ?>
									<?php while (have_posts()) : the_post(); ?>
				                    <li class="splide__slide">
				                    	<a href="<?php the_permalink(); ?>" class="news-links text-decoration-none">
				                    		<?php if ( function_exists( 'add_theme_support' ) )
												the_post_thumbnail( array(526,9999), array('class' => 'img-fluid') ); 
											?>
		                                    <div class="news-info row">
		                                        <span class="d-inline-block text-truncate text-white" style="max-width: 348px;">
		                                           <?php the_title(); ?>
		                                        </span>
		                                        <p class="text-white mt-1 mb-0">
		                                            <?php echo get_the_date('d.m.Y'); ?>
		                                        </p>
		                                    </div>
				                    	</a>
	                                </li>
				                    <?php endwhile; ?>
									<?php endif; wp_reset_query(); ?>
	                            </ul>
	                        </div>
	                        <div class="splide__arrows d-none">
	                            <button class="splide__arrow splide__arrow--prev">
	                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
	                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
	                                </svg>
	                            </button>
	                            <button class="splide__arrow splide__arrow--next">
	                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
	                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
	                                </svg>
	                            </button>
	                        </div>
	                    </div>
	                </section>
	                <a href="/news" class="button-primary my-5" style="max-width: 288px;margin: 0 auto;">Все новости</a>
            	</div>
            	
            </div>
        </div>
    </div>
</section>



<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
        <h2>Наши партнеры</h2>
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>

</main><!-- .site-main -->

<?php get_footer(); ?>
