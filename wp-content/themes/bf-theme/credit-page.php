<?php
/**
 * Template Name: Шаблон страницы кредитов
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
	
	<style>
		.page-id-34 .banner {
			background-size: cover;
			background-position: 68%;
		}
		.page-id-32 .banner {
			background-size: cover;
			background-position: 37%;
		}
		.page-id-36 .banner {
			background-size: cover;
			background-position: 68%;
		}
		.page-id-38 .banner {
			background-size: cover;
			background-position: 68%;
		}
	</style>

<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php the_title(); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

            <div class="banner-menu">
            	<?php 
					$link = get_field('ssylka_1');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_2');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_3');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
            </div>

        </div>
    </div>
</section>


<section class="credits">
    <div class="petal-icon"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xl-8 p-md-0 m-md-0">
                <div class="credits-scroll">
                    <ul class="nav nav-tabs credits-tabs" id="myTab" role="tablist">
                    	<?php if( have_rows('tab_1') ): ?>
						    <?php while( have_rows('tab_1') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tab-content-1" type="button" role="tab" aria-controls="tab-content-1" aria-selected="true">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
                        <?php if( have_rows('tab_2') ): ?>
						    <?php while( have_rows('tab_2') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-2" data-bs-toggle="tab" data-bs-target="#tab-content-2" type="button" role="tab" aria-controls="tab-content-2" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_3') ): ?>
						    <?php while( have_rows('tab_3') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-3" data-bs-toggle="tab" data-bs-target="#tab-content-3" type="button" role="tab" aria-controls="tab-content-3" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_4') ): ?>
						    <?php while( have_rows('tab_4') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-4" data-bs-toggle="tab" data-bs-target="#tab-content-4" type="button" role="tab" aria-controls="tab-content-4" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_5') ): ?>
						    <?php while( have_rows('tab_5') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-5" data-bs-toggle="tab" data-bs-target="#tab-content-5" type="button" role="tab" aria-controls="tab-content-6" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_6') ): ?>
						    <?php while( have_rows('tab_6') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-6" data-bs-toggle="tab" data-bs-target="#tab-content-6" type="button" role="tab" aria-controls="tab-content-6" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
                    </ul>
                </div>
                <div class="tab-content credits-tabs-content" id="myTabContent">
                	
                	<?php if( have_rows('tab_1') ): ?>
					    <?php while( have_rows('tab_1') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">
			                        <div class="d-flex align-items-center justify-content-between">
			                            <p class="credits-tabs-content__title">
			                                <?php the_sub_field('opisanie'); ?>
			                            </p>
			                            <div class="dropdown credits-dropdown">
			                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
			                                    Цель кредита
			                                </button>
			                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
			                                    <li class="p-3"><?php the_sub_field('czel_kredita'); ?></li>
			                                </ul>
			                            </div>
			                        </div>
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_2') ): ?>
					    <?php while( have_rows('tab_2') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
			                        <div class="d-flex align-items-center justify-content-between">
			                            <p class="credits-tabs-content__title">
			                                <?php the_sub_field('opisanie'); ?>
			                            </p>
			                            <div class="dropdown credits-dropdown">
			                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
			                                    Цель кредита
			                                </button>
			                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
			                                    <li class="p-3"><?php the_sub_field('czel_kredita'); ?></li>
			                                </ul>
			                            </div>
			                        </div>
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_3') ): ?>
					    <?php while( have_rows('tab_3') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fadee" id="tab-content-3" role="tabpanel" aria-labelledby="tab-3">
			                        <div class="d-flex align-items-center justify-content-between">
			                            <p class="credits-tabs-content__title">
			                                <?php the_sub_field('opisanie'); ?>
			                            </p>
			                            <div class="dropdown credits-dropdown">
			                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
			                                    Цель кредита
			                                </button>
			                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
			                                    <li class="p-3"><?php the_sub_field('czel_kredita'); ?></li>
			                                </ul>
			                            </div>
			                        </div>
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_4') ): ?>
					    <?php while( have_rows('tab_4') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-4" role="tabpanel" aria-labelledby="tab-4">
			                        <div class="d-flex align-items-center justify-content-between">
			                            <p class="credits-tabs-content__title">
			                                <?php the_sub_field('opisanie'); ?>
			                            </p>
			                            <div class="dropdown credits-dropdown">
			                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
			                                    Цель кредита
			                                </button>
			                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
			                                    <li class="p-3"><?php the_sub_field('czel_kredita'); ?></li>
			                                </ul>
			                            </div>
			                        </div>
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_5') ): ?>
					    <?php while( have_rows('tab_5') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-5" role="tabpanel" aria-labelledby="tab-5">
			                        <div class="d-flex align-items-center justify-content-between">
			                            <p class="credits-tabs-content__title">
			                                <?php the_sub_field('opisanie'); ?>
			                            </p>
			                            <div class="dropdown credits-dropdown">
			                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
			                                    Цель кредита
			                                </button>
			                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
			                                    <li class="p-3"><?php the_sub_field('czel_kredita'); ?></li>
			                                </ul>
			                            </div>
			                        </div>
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_6') ): ?>
					    <?php while( have_rows('tab_6') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-6" role="tabpanel" aria-labelledby="tab-6">
			                        <div class="d-flex align-items-center justify-content-between">
			                            <p class="credits-tabs-content__title">
			                                <?php the_sub_field('opisanie'); ?>
			                            </p>
			                            <div class="dropdown credits-dropdown">
			                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
			                                    Цель кредита
			                                </button>
			                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
			                                    <li class="p-3"><?php the_sub_field('czel_kredita'); ?></li>
			                                </ul>
			                            </div>
			                        </div>
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
                	
                	
                	
                    
                </div>
            </div>
            <div class="col-sm-12 col-xl-4">
                <div class="credit-container__left credit-container__horizontal">
                    <h5 class="text-center">Кредитный <span class="text__red-dark">калькулятор</span></h5>

                    <div class="row">
                        <div class="col-6 text-start">
                            <label for="selectCredit" class="d-block mb-2">Тип кредита</label>
                            <select name="" id="selectCredit">
                                <option value="30">Здоровье</option>
                                <option value="35">Улучшение жилья</option>
                                <option value="38">Агро кредит</option>
                                <option value="28">Бизнес кредит</option>
                                <option value="25">Мурабаха</option>
                                <option value="45">Миң түркүн</option>
                                <option value="22">ВИЭ Кредит</option>
                                <option value="32">ЦДС Кредит</option>
                                <option value="28">Потребительский</option>
                            </select>
                        </div>
                        <div class="col-6 text-end">
                            <label class="d-block mb-2" style="font-size: 15px;">Процентная ставка %</label>
                            <input type="number" min="1" max="100" id="percentage-block" value="30" class="percentage-block ms-auto">
                        </div>
                        <div class="col-12 mt-4">
                            <div class="ty-control-group slider_universal-block">
                                <div class="d-flex align-items-center justify-content-between mb-3">
                                    <label for="slider_universal" class="m-0">Выберите размер кредита</label>
                                    <p class="slider_universal_texts_payment">
                                        <input type="number" min="5000" max="50000" id="universal_hidden" name="calc_slider" value="50000" />
                                        <span>c</span>
                                    </p>
                                </div>
                                <div id="slider_universal" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"></div>
                                <div class="slider_universal_terms">
                                    <div id="slider_universal_min_term">5000</div>
                                    <div id="slider_universal-result">25000</div>
                                    <div id="slider_universal_max_term">50000</div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 mt-4">
                            <div class="ty-control-group slider_universal-block-2">
                                <div class="d-flex align-items-center justify-content-between mb-3">
                                    <p>
                                        <label for="slider_universal_min_payment-hidden" class="m-0">Выберите срок (мес)</label>
                                    </p>
                                    <p class="slider_universal_texts_payment slider_day">
                                        <input type="number" min="1" max="18" id="slider_universal_min_payment-hidden" name="calc_slider2" value="12">
                                    </p>
                                </div>

                                <div id="slider_universal_min_payment" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"></div>
                                <div class="slider_universal_first_p">
                                    <div id="slider_universal_min_first_count">1 мес</div>
                                    <div id="slider_universal_min_count-result">12 мес</div>
                                    <div id="slider_universal_max_first_count">18 мес</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="row mt-5">
                        <div class="col-6">
                            <div class="calc_result">
                                <div class="result_item">
                                    <p>Ежемесячный платеж</p>
                                    <p id="price">4874</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="result_item text-end">
                                <p>Процентная ставка</p>
                                <p id="initpay" class="m-0">30 %</p>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="result_item">
                                <p>Сумма кредита</p>
                                <p id="allSum" class="m-0">58492</p>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="result_item m-0 text-end">
                                <p>Срок кредита</p>
                                <p id="allDay" class="m-0">12</p>
                            </div>
                        </div>
                        <div class="col-12 mt-4 mb-3">
                            <a id="myAnchor" href="/grafik-pogasheniya-kredita/?summ=58492&srok=12&proc=30" target="_blank" class="button-primary button-primary__outline w-100">
                            	График погашения кредита
                            </a>
                        </div>
                        <div class="col-12">
                            <a href="#1"
                               class="button-secondary button-secondary__outline w-100 calc_result--btn"
                               data-bs-toggle="modal"
                               data-bs-target="#calcModal"
                               data-credit="58492"
                               data-month="12"
                               data-procent="30">
                                Оставить заявку
                            </a>
                            <!--<a href="/pogashenie-kredita/"
                               class="button-secondary button-secondary__outline w-100 calc_result--btn mt-3"
                            >
                                Способы погашения кредита
                            </a>-->
                            <p class="calc_result--info mt-4">
                                Пример расчёта процентной ставки носит исключительно информационный характер и не является публичной офертой
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container mt-md-5">
        <div class="question-container">
            <h4 class="text-center">Остались <span class="text__red-dark text-uppercase">ВОПРОСЫ?</span></h4>
            <p class="text-center">Вы можете оставить заявку на получение консультации от менеджера продаж, используя данную форму</p>
            <?php echo do_shortcode('[contact-form-7 id="273" title="Контактная форма 1"]'); ?>
        </div>
    </div>
</section>


<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<h2>Partners</h2>
    	<?php } else { ?>
    		<h2>Наши партнеры</h2>
    	<?php } ?>
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>

</main><!-- .site-main -->

<?php get_footer(); ?>
