
<?php get_header(); ?>
<?php echo esc_url( get_template_directory_uri() ); ?>

	<!-- END Our Partners -->
<section id="news" class="news">
	<div class="container">
		<div class="row">	
			
			<div class="col-xs-12 col-md-8">
				<h2 class="text-center h2">События и Новости</h2>
              <?php $params = array('posts_per_page' => 4, 'cat' => array(2,-24) ); 
            	    if (have_posts()) : query_posts($params); ?>
				<?php while (have_posts()) : the_post(); ?>
					<div class="row row-news">
						<a href="<?php the_permalink(); ?>" class="img-link">
							<?php if ( has_post_thumbnail()) { ?>
                         	 <div class="col-xs-3 col-news-1">
								<figure>
									<?php 
										if ( function_exists( 'add_theme_support' ) )
										the_post_thumbnail( array(166,9999), array('class' => 'img-responsive') ); 
									?>
								</figure>
							</div>
              				
							<div class="col-xs-9 col-news-2">
								<!--<div class="post-date"><?php echo get_the_date('d.m.Y'); ?></div>-->
								<h3 class="header-news"><?php the_title(); ?></h3>
								<p style="color:#4F7195;"><?php echo get_the_date('d.m.Y'); ?></p>
								<div class="post-content">
									<?php the_excerpt(); ?>
								</div>
							</div>
                          <?php } else { ?>
                          <div class="col-xs-12 col-news-2">
								<!--<div class="post-date"><?php echo get_the_date('d.m.Y'); ?></div>-->
								<h3 class="header-news"><?php the_title(); ?></h3>
								<p style="color:#4F7195;"><?php echo get_the_date('d.m.Y'); ?></p>
								<div class="post-content">
									<?php the_excerpt(); ?>
								</div>
							</div>
                          <?php  } ?>
						</a>
					</div>
				<?php endwhile; ?>

				<?php else : ?>
				<?php endif; wp_reset_query(); ?>
				
				<div class="row">
                  <div class="col-xs-12 text-center">
                      <a href="<?php echo get_site_url(); ?>/vse-zapisi/" class="btn btn-default btn-lg btn-news">Все новости</a>
                  </div>
              </div>
			</div>
			<div class="col-xs-12 col-md-4 hidden-xs">
				<div class="row">
					<div class="col-xs-12">
						<?php get_sidebar( 'Правый сайдбар' ); ?>
					</div>
				</div>
			</div>
			
			
		</div>
		
	</div>
</section>   

<?php get_footer(); ?>