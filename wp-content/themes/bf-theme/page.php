<?php
/**
 * The template for displaying pages
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
	
	
	<style>
		@media(max-width: 640px) {
			.sp-easy-accordion .sp-ea-single .ea-header a{
				font-size: 14px !important;
    line-height: normal!important;
			}
		}
	</style>
	
	

<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { the_field('fon_dlya_straniczy'); } else { ?>/wp-content/themes/bf-theme/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php the_title(); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

			<?php $link = get_field('ssylka_1'); if( $link ) { ?>
	            <div class="banner-menu">
	            	<?php 
						$link = get_field('ssylka_1');
						if( $link ): 
						    $link_url = $link['url'];
						    $link_title = $link['title'];
						    $link_target = $link['target'] ? $link['target'] : '_self';
						    ?>
					    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
					    	<?php echo esc_html( $link_title ); ?>
					    </a>
					<?php endif; ?>
					<?php 
						$link = get_field('ssylka_2');
						if( $link ): 
						    $link_url = $link['url'];
						    $link_title = $link['title'];
						    $link_target = $link['target'] ? $link['target'] : '_self';
						    ?>
					    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
					    	<?php echo esc_html( $link_title ); ?>
					    </a>
					<?php endif; ?>
					<?php 
						$link = get_field('ssylka_3');
						if( $link ): 
						    $link_url = $link['url'];
						    $link_title = $link['title'];
						    $link_target = $link['target'] ? $link['target'] : '_self';
						    ?>
					    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
					    	<?php echo esc_html( $link_title ); ?>
					    </a>
					<?php endif; ?>
	            </div>
            <?php } ?>

        </div>
    </div>
</section>



<?php if( have_rows('tab_1') ) { ?>
<section class="credits">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 p-md-0 m-md-0">
                <div class="credits-scroll">
                    <ul class="nav nav-tabs credits-tabs" id="myTab" role="tablist">
                    	<?php if( have_rows('tab_1') ): ?>
						    <?php while( have_rows('tab_1') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tab-content-1" type="button" role="tab" aria-controls="tab-content-1" aria-selected="true">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
                        <?php if( have_rows('tab_2') ): ?>
						    <?php while( have_rows('tab_2') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-2" data-bs-toggle="tab" data-bs-target="#tab-content-2" type="button" role="tab" aria-controls="tab-content-2" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_3') ): ?>
						    <?php while( have_rows('tab_3') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-3" data-bs-toggle="tab" data-bs-target="#tab-content-3" type="button" role="tab" aria-controls="tab-content-3" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_4') ): ?>
						    <?php while( have_rows('tab_4') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-4" data-bs-toggle="tab" data-bs-target="#tab-content-4" type="button" role="tab" aria-controls="tab-content-4" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_5') ): ?>
						    <?php while( have_rows('tab_5') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-5" data-bs-toggle="tab" data-bs-target="#tab-content-5" type="button" role="tab" aria-controls="tab-content-6" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_6') ): ?>
						    <?php while( have_rows('tab_6') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-6" data-bs-toggle="tab" data-bs-target="#tab-content-6" type="button" role="tab" aria-controls="tab-content-6" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_7') ): ?>
						    <?php while( have_rows('tab_7') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-7" data-bs-toggle="tab" data-bs-target="#tab-content-7" type="button" role="tab" aria-controls="tab-content-7" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
                    </ul>
                </div>
                <div class="tab-content credits-tabs-content" id="myTabContent">
                	
                	<?php if( have_rows('tab_1') ): ?>
					    <?php while( have_rows('tab_1') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_2') ): ?>
					    <?php while( have_rows('tab_2') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_3') ): ?>
					    <?php while( have_rows('tab_3') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fadee" id="tab-content-3" role="tabpanel" aria-labelledby="tab-3">
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_4') ): ?>
					    <?php while( have_rows('tab_4') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-4" role="tabpanel" aria-labelledby="tab-4">
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_5') ): ?>
					    <?php while( have_rows('tab_5') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-5" role="tabpanel" aria-labelledby="tab-5">
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_6') ): ?>
					    <?php while( have_rows('tab_6') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-6" role="tabpanel" aria-labelledby="tab-6">
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_7') ): ?>
					    <?php while( have_rows('tab_7') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-7" role="tabpanel" aria-labelledby="tab-7">
			                        <div class="credits-table">
			                            <?php the_sub_field('kontent'); ?>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<section class="page-section <?php if(!is_page(363)) { ?>py-5<?php } ?>">
    <div class="petal-icon"></div>
    <div class="petal-icon__left"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
               <?php the_content(); ?>
            </div>
            <?php if(is_page(30)) { ?>
				 <div class="text-end ">
                    <button class="ms-auto my-5 button-primary" data-bs-toggle="modal" data-bs-target="#vacModal">
                        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							<h2>Send file (.doc или docx)</h2>
						<?php } else { ?>
							<h2>Отправить резюме (.doc или docx)</h2>
						<?php } ?>
                    </button>
                </div>
			<?php } ?>
        </div>
    </div>
</section>


<?php if(is_page(30)) { ?>
<!-- VACATION MODAL -->
<div class="modal fade" id="vacModal" tabindex="-1" aria-labelledby="vacModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="vacModalLabel">Отправить резюме</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-xl-6">
                        <div class="form-group">
                            <label class="col-form-label">Имя</label>
                            <div class="form-group your-name">
                                <input class="form-control" name="your-name" type="text" value="" placeholder="Example" aria-invalid="false" aria-required="true" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-xl-6">
                        <div class="form-group">
                            <label class="col-form-label">E-mail</label>
                            <div class="form-group your-email">
                                <input class="form-control" name="your-email" type="email" value="" placeholder="Example@domain.com" aria-invalid="false" aria-required="true" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-xl-6  mt-3">
                        <div class="form-group">
                            <label class="col-form-label">Телефон</label>
                            <div class="form-group tel-479">
                                <input class="form-control" name="tel-479" type="tel" value="" placeholder="+996 (559) 991 111" aria-invalid="false" aria-required="true" required></div>
                        </div>
                    </div>

                    <div class="col-12  col-xl-6 mt-3">
                        <div class="form-group">
                            <label class="col-form-label">Позиция</label>
                            <div class="form-group pos-1">
                                <input class="form-control" name="pos-1" type="text" placeholder="финансист" aria-invalid="false" aria-required="true" required></div>
                        </div>
                    </div>

                    <div class="col-12 mt-3">
                        <div class="form-group">
                            <label for="formFile" class="form-label">Прикрепить файл </label>
                            <input class="form-control" type="file" id="formFile" accept=".doc, .docx, .pdf, .ptt, .pttx ">
                        </div>
                    </div>



                    <div class="col-12 mt-3">
                        <div class="form-group">
                            <label class="col-form-label">Комментарии</label>
                            <div class="form-group your-message">
                                <textarea class="form-control" name="your-message" rows="4" placeholder="Текст..." aria-invalid="false"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 text-center mt-3">
                        <div class="form-group">
                            <div>
                                <input class="button-primary m-auto" type="submit" value="Отправить">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div class="container mt-md-5">
    <div class="question-container">
        <h4 class="text-center">Остались <span class="text__red-dark text-uppercase">ВОПРОСЫ?</span></h4>
        <p class="text-center">Вы можете оставить заявку на получение консультации от менеджера продаж, используя данную форму</p>
        <?php echo do_shortcode('[contact-form-7 id="273" title="Контактная форма 1"]'); ?>
    </div>
</div>

<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
    	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<h2>Partners</h2>
    	<?php } else { ?>
    		<h2>Наши партнеры</h2>
    	<?php } ?>
        
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>

</main><!-- .site-main -->

<?php get_footer(); ?>
