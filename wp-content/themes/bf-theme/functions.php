<?php
 // Register custom navigation walker
    require_once('wp_bootstrap_navwalker.php');
	
# Breadcrumb
function the_breadcrumb() {
	if (!is_home()) {
		echo '<li><a href="';
		echo get_option('home').'">';
		echo 'Главная';
		echo "</a> <span class='active'></span></li> ";
		if (is_category() || is_single()) {
			echo "<li>";
			single_cat_title();
			echo "</li>";
			if (is_single()) {
			the_category(', ');
				echo " <span>/ </span><li class='active'> ";
				the_title();
				echo "</li>";
			}
		} elseif (is_page()) {
			echo "<li>";
			echo the_title();
			echo "</li>";
		}
		  elseif (is_tag()) {
			echo 'Записи с меткой "'; 
			single_tag_title();
			echo '"'; }
		elseif (is_day()) {echo "Архив за"; the_time('  jS F Y');}
		elseif (is_month()) {echo "Архив "; the_time(' F  Y');}
		elseif (is_year()) {echo "Архив за "; the_time(' Y');}
		elseif (is_author()) {echo "Архив автора";}
		elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "Архив блога";}
		elseif (is_search()) {echo "Результаты поиска";}
			elseif (is_404()) {	echo '404 - Страница не найдена';}
	}
}


//Произвольное меню
if ( function_exists( 'register_nav_menus' ) )
{
	register_nav_menus(
		array(
			'custom-menu'=>__('Custom menu'),
			'right-menu'=>__('Right menu'),
			'header-menu'=>__('Header menu'),
			'big-menu'=>__('Big menu'),
			'footer-menu'=>__('Footer menu'),
		)
	);
}

function custom_menu(){
	echo '<ul>';
		wp_list_pages('title_li=&');
	echo '</ul>';
}

// Регистрируем сайдбары
if ( function_exists('register_sidebar') ) {
	
    register_sidebar(array(
    	'id' => 'sidebar-1',
		'name' => 'Right sidebar',
		'before_widget' => '<div class="widget">',
        'before_title' => '<h2 class="sidebar-header text-center">',
        'after_title' => '</h2><div class="text">',
        'after_widget' => '</div></div>'
	));
	
	register_sidebar(array(
		'id' => 'sidebar-2',
		'name' => 'Footer sidebar',
		'before_widget' => '<div class="widget">',
        'before_title' => '<h2 class="sidebar-header text-center">',
        'after_title' => '</h2><div class="text">',
        'after_widget' => '</div></div>'
	));
	
	register_sidebar(array(
		'id' => 'sidebar-3',
		'name' => 'Header sidebar',
		'before_widget' => '<div class="widget">',
        'before_title' => '<h2>',
        'after_title' => '</h2><div class="text">',
        'after_widget' => '</div></div>'
	));
	
	register_sidebar(array(
		'id' => 'sidebar-4',
		'name' => 'Gallery sidebar',
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>'
	));
	
}
show_admin_bar(false);
remove_action( 'wp_head', 'feed_links_extra', 3 ); 
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );

add_theme_support( 'post-thumbnails' );

//add_filter('template_include', 'my_template');
// функция фильтрации
//function my_template( $template ) {
	# шаблон для группы рубрик
	//if( is_category( '3' ) ){
	//	return get_stylesheet_directory() . '/serials.php';
	//}
//}

add_filter( 'avatar_defaults', 'newgravatar' );
 
function newgravatar ($avatar_defaults) {
$myavatar = get_bloginfo('template_directory') . '/images/avatar_default.png';
$avatar_defaults[$myavatar] = "Новый аватар";
return $avatar_defaults;
}

function new_excerpt_length($length) {
	return 20;
}
add_filter('excerpt_length', 'new_excerpt_length');

add_filter('excerpt_more', function($more) {
	return ' ...';
});

function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

function title_limit($count, $after) {
$title = get_the_title();
if (mb_strlen($title) - $count) $title = mb_substr($title,0,$count);
else $after = '';
echo $title . $after;
}

// PARTNERS

add_action( 'init', 'create_partners_taxonomies' );

function create_partners_taxonomies() {
	
	register_taxonomy('category-partners', array('partners'), array(
		'hierarchical'  => true,
		'labels'        => array(
			'name'              => _x( 'Partners', 'taxonomy general name' ),
			'singular_name'     => _x( 'Partner', 'taxonomy singular name' ),
			'search_items'      =>  __( 'Search Partners' ),
			'all_items'         => __( 'All Partners' ),
			'parent_item'       => __( 'Parent Partner' ),
			'parent_item_colon' => __( 'Parent Partner:' ),
			'edit_item'         => __( 'Edit Partner' ),
			'update_item'       => __( 'Update Partner' ),
			'add_new_item'      => __( 'Add New Partner' ),
			'new_item_name'     => __( 'New Partner Name' ),
			'menu_name'         => __( 'Категория' ),
		),
		'show_ui'       => true,
		'query_var'     => true,
		//'rewrite'       => array( 'slug' => 'the_genre' ), // свой слаг в URL
	));
	
	// // Добавляем НЕ древовидную таксономию 'writer' (как метки)
	// register_taxonomy('taxonomy-partners', 'partners',array(
	// 	'hierarchical'  => false,
	// 	'labels'        => array(
	// 		'name'                        => _x( 'Partners', 'taxonomy general name' ),
	// 		'singular_name'               => _x( 'Partner', 'taxonomy singular name' ),
	// 		'search_items'                =>  __( 'Search Partners' ),
	// 		'popular_items'               => __( 'Popular Partners' ),
	// 		'all_items'                   => __( 'All Partners' ),
	// 		'parent_item'                 => null,
	// 		'parent_item_colon'           => null,
	// 		'edit_item'                   => __( 'Edit Partner' ),
	// 		'update_item'                 => __( 'Update Partner' ),
	// 		'add_new_item'                => __( 'Add New Partner' ),
	// 		'new_item_name'               => __( 'New Partner Name' ),
	// 		'separate_items_with_commas'  => __( 'Separate Partner with commas' ),
	// 		'add_or_remove_items'         => __( 'Add or remove Partner' ),
	// 		'choose_from_most_used'       => __( 'Choose from the most used Partners' ),
	// 		'menu_name'                   => __( 'Таксономия' ),
	// 	),
	// 	'show_ui'       => true,
	// 	'query_var'     => true,
	// 	//'rewrite'       => array( 'slug' => 'the_writer' ), // свой слаг в URL
	// ));
}

add_action('init', 'partners_post_type');

function partners_post_type() {
	register_post_type('partners', array(
		'labels'            => array(
			'name'          => 'Партнеры',
			'singular_name' => 'Партнеры',
			'menu_name'     => 'Партнеры',
			'menu_icon'     => 'dashicons-universal-access',
			
		),
		'public' => true,
	));
}



// TEAMS

add_action( 'init', 'create_teams_taxonomies' );

function create_teams_taxonomies() {
	
	register_taxonomy('category-teams', array('teams'), array(
		'hierarchical'  => true,
		'labels'        => array(
			'name'              => _x( 'teams', 'taxonomy general name' ),
			'singular_name'     => _x( 'team', 'taxonomy singular name' ),
			'search_items'      =>  __( 'Search teams' ),
			'all_items'         => __( 'All teams' ),
			'parent_item'       => __( 'Parent teams' ),
			'parent_item_colon' => __( 'Parent teams:' ),
			'edit_item'         => __( 'Edit teams' ),
			'update_item'       => __( 'Update teams' ),
			'add_new_item'      => __( 'Add New teams' ),
			'new_item_name'     => __( 'New teams Name' ),
			'menu_name'         => __( 'Категория' ),
		),
		'show_ui'       => true,
		'query_var'     => true,
	));
	
}

add_action('init', 'teams_post_type');

function teams_post_type() {
	register_post_type('teams', array(
		'labels'            => array(
			'name'          => 'Сотрудники',
			'singular_name' => 'Сотрудники',
			'menu_name'     => 'Сотрудники',
			'menu_icon'     => 'dashicons-universal-access',
			
		),
		'public' => true,
	));
}



// MAIN CAROUSEL

add_action( 'init', 'create_mainCarousel_taxonomies' );

function create_mainCarousel_taxonomies() {
	
	register_taxonomy('category-mainCarousel', array('mainCarousel'), array(
		'hierarchical'  => true,
		'labels'        => array(
			'name'              => _x( 'mainCarousel', 'taxonomy general name' ),
			'singular_name'     => _x( 'mainCarousel', 'taxonomy singular name' ),
			'search_items'      =>  __( 'Search mainCarousel' ),
			'all_items'         => __( 'All mainCarousel' ),
			'parent_item'       => __( 'Parent mainCarousel' ),
			'parent_item_colon' => __( 'Parent mainCarousel:' ),
			'edit_item'         => __( 'Edit mainCarousel' ),
			'update_item'       => __( 'Update mainCarousel' ),
			'add_new_item'      => __( 'Add New mainCarousel' ),
			'new_item_name'     => __( 'New mainCarousel Name' ),
			'menu_name'         => __( 'Категория' ),
		),
		'show_ui'       => true,
		'query_var'     => true,
	));
	
}

add_action('init', 'mainCarousel_post_type');

function mainCarousel_post_type() {
	register_post_type('mainCarousel', array(
		'labels'            => array(
			'name'          => 'Карусель',
			'singular_name' => 'Карусель',
			'menu_name'     => 'Карусель',
			'menu_icon'     => 'dashicons-universal-access',
			
		),
		'public' => true,
	));
}




?>