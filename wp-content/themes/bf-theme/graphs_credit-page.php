<?php
/**
 * Template Name: Шаблон страницы графика кредитов
 */

?>


<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="utf-8"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@400;600;700&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/reset-css@5.0.1/reset.min.css" />
<style>
	

html {
  height: 100%;
}
body {
  height: 100%;
  font: 18px/20px 'Titillium Web', sans-serif;
  background: #fff;
  width: 100%;
}
img {
  max-width: 100%;
}
.wrapper {
  margin: 0 auto;
  max-width: 1290px;
  padding-bottom: 50px;
  padding: 0 20px;
  min-width: 700px;
}
.print {
    text-align: center;
    padding: 12px 31px;
    font-weight: normal;
    font-size: 18px;
    line-height: 20px;
    color: #fff;
    text-decoration: none;
    border: 1px solid ;
    box-shadow: 0 1px 4px rgb(0 0 0 / 25%);
    background: #c30019;
    border-color: #c30019;
    border-radius: 5px;
  margin-bottom: 20px;
  display: inline-block;
  float: right;
  transition: all .2s ease;
}
.print:hover,
.print:active,
.print:focus {
  background: transparent;
  color: #c30019;
}
.header {
  height: 120px;
}
.header .logo {
  padding-top: 12px;
}
.header .phone {
  background: url('/static/ima/phone.png') no-repeat 0 93%;
  padding-left: 50px;
  font-family: 'proxima';
  line-height: 48px;
  padding-top: 30px;
  font-weight: bold;
  font-size: 48px;
  color: #bc0a29;
}
.header .phone span {
  display: block;
  color: #000000;
  font-size: 18px;
  line-height: 18px;
  margin-bottom: -5px;
}
.datatable {
  margin-bottom: 20px;
  width: 100%;
}
.datatable .itogo {
  font-weight: bold;
  color: #bc0a29;
  font-size: 18px;
}
.datatable th {
  padding: 7px 10px;
  font-weight: bold;
}
.datatable tr.odd {
  background: #e7e7e7;
}
.datatable td {
  padding: 7px 10px;
  text-align: center;
  border-top: 1px solid #bc0a29;
}
.headd {
  padding: 0 0 25px 0;
  font-size: 20px;
}
.headd span {
  display: inline-block;
  margin-right: 20px;
  font-weight: bold;
  color: #bc0a29;
}
</style>
    <script src="https://code.jquery.com/jquery-1.8.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script>
	    calctrans = {
	        som: 'сом',
	        insertnumber: 'Введите число',
	        itogo: 'Итого',
	    }
	</script>    
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/calcCred.js"></script>
    
    
    <title>График погашения кредита</title>
</head>
<body>
<div class="wrapper">
    <header class="header">
        <div class="row">
            <div class="left logo">
                <a href="<?php if ( WPGlobus::Config()->language == 'ru' ) { ?>/<?php } else { ?>/en/<?php } ?>" class="logo-link d-flex align-items-center text-decoration-none me-xl-auto">
		            <?php if ( WPGlobus::Config()->language == 'ru' ) { ?>
		        	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo/new-logo.svg" alt="" style="max-width: 190px;">
		        	<?php } else { ?>
		        	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo/new-logo-eng.svg" alt="" style="max-width: 190px;">
		        	<?php } ?>
		        </a>
            </div>
        </div>
    </header>
    
    <div style="clear: right"></div>
    <div class="headd">
    	Ежемесячный платеж: <span id="emonth"></span>
    	Общая сумма выплат: <span id="eplat"></span>
    	Сумма: <span id="esumm">50000 сом</span>
    	Срок: <span id="emonth1">18 месяцев</span>
    	Процентная ставка: <span id="eproc">30 %</span>
    </div>
    <table class="datatable" id="datatable" cellspacing="0">
        <tr>
            <th>Номер выплаты</th>
            <th class="data">Дата погашения</th>
            <th>Остаток основного долга</th>
            <th>Погашение вознаграждения</th>
            <th>Погашение основного долга</th>
            <th>Платеж по кредиту</th>
        </tr>
    </table>
    <a href="javascript:window.print();" class="print">Распечатать</a>

    <div style="clear: right"></div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
    	const queryString = location.search;
    	const params = new URLSearchParams(queryString);
		const summ = parseInt(params.get("summ"));
		const srok = parseInt(params.get("srok"));
		const proc = parseInt(params.get("proc"));
		document.querySelector('#esumm').innerHTML = summ;
		document.querySelector('#emonth1').innerHTML = srok;
		document.querySelector('#eproc').innerHTML = proc;
        calcRender(summ,srok,proc);
    });
</script>



</body>
</html>


