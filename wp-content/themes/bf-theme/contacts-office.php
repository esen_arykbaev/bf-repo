<?php
/**
 * Template Name: Шаблон страницы офисов
 */
?>
<?php get_header(); ?>
<main id="main" class="site-main" role="main">

<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php the_title(); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

            <div class="banner-menu">
            	<?php 
					$link = get_field('ssylka_1');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_2');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_3');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
            </div>

        </div>
    </div>
</section>

<section class="office">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="credits-scroll">
                    <ul class="nav nav-tabs credits-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tab-content-1" type="button" role="tab" aria-controls="tab-content-1" aria-selected="true">
                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Chui region
						    	<?php } else { ?>
						    		Чуйская область
						    	<?php } ?>
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="tab-2" data-bs-toggle="tab" data-bs-target="#tab-content-2" type="button" role="tab" aria-controls="tab-content-2" aria-selected="false">
                                
                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Naryn region
						    	<?php } else { ?>
						    		Нарынская <br>
                                область
						    	<?php } ?>
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="tab-3" data-bs-toggle="tab" data-bs-target="#tab-content-3" type="button" role="tab" aria-controls="tab-content-3" aria-selected="false">
                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Djalal-Abad region
						    	<?php } else { ?>
						    		Джалал-абадская <br>
                                область
						    	<?php } ?>
                                
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="tab-4" data-bs-toggle="tab" data-bs-target="#tab-content-4" type="button" role="tab" aria-controls="tab-content-4" aria-selected="false">
                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Osh region
						    	<?php } else { ?>
						    		Ошская <br>
                                область
						    	<?php } ?>
                                
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="tab-5" data-bs-toggle="tab" data-bs-target="#tab-content-5" type="button" role="tab" aria-controls="tab-content-5" aria-selected="false">
                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Batken region
						    	<?php } else { ?>
						    		Баткенская<br>
                                область
						    	<?php } ?>
                                
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="tab-6" data-bs-toggle="tab" data-bs-target="#tab-content-6" type="button" role="tab" aria-controls="tab-content-6" aria-selected="false">
                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Issy-Kul region
						    	<?php } else { ?>
						    		Иссык-Кульская<br>
                                область
						    	<?php } ?>
                                
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="tab-7" data-bs-toggle="tab" data-bs-target="#tab-content-7" type="button" role="tab" aria-controls="tab-content-7" aria-selected="false">
                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Talas region
						    	<?php } else { ?>
						    		Таласская<br>
                                область
						    	<?php } ?>
                                
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-content credits-tabs-content" id="myTabContent">
            <div class="tab-pane fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">
                <div class="row">
                    <div class="col-12 col-xl-8">
                        <div class="office-maps">
                        	<?php echo do_shortcode('[put_wpgm id=3]'); ?>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4">
                        <aside class="office-aside">
                            <h5 class="text-center mb-4">
                            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Offices in your area:
						    	<?php } else { ?>
						    		Офисы в вашем регионе:
						    	<?php } ?>
                            </h5>
                            <?php echo do_shortcode('[sp_easyaccordion id="422"]'); ?>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
                <div class="row">
                    <div class="col-12 col-xl-8">
<?php echo do_shortcode('[put_wpgm id=4]'); ?>
                    </div>
                    <div class="col-sm-12 col-xl-4">
                        <aside class="office-aside">
                            <h5 class="text-center mb-4">
                            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Offices in your area:
						    	<?php } else { ?>
						    		Офисы в вашем регионе:
						    	<?php } ?>
                            </h5>
                            <?php echo do_shortcode('[sp_easyaccordion id="452"]'); ?>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-content-3" role="tabpanel" aria-labelledby="tab-3">
                <div class="row">
                    <div class="col-12 col-xl-8">
<?php echo do_shortcode('[put_wpgm id=5]'); ?>
                    </div>
                    <div class="col-sm-12 col-xl-4">
                        <aside class="office-aside">
                            <h5 class="text-center mb-4">
                            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Offices in your area:
						    	<?php } else { ?>
						    		Офисы в вашем регионе:
						    	<?php } ?>
                            </h5>
                            <?php echo do_shortcode('[sp_easyaccordion id="453"]'); ?>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-content-4" role="tabpanel" aria-labelledby="tab-4">
                <div class="row">
                    <div class="col-12 col-xl-8">
<?php echo do_shortcode('[put_wpgm id=6]'); ?>
                    </div>

                    <div class="col-sm-12 col-xl-4">
                        <aside class="office-aside">
                            <h5 class="text-center mb-4">
                            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Offices in your area:
						    	<?php } else { ?>
						    		Офисы в вашем регионе:
						    	<?php } ?>
                            </h5>
                            <?php echo do_shortcode('[sp_easyaccordion id="454"]'); ?>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-content-5" role="tabpanel" aria-labelledby="tab-5">
                <div class="row">
                    <div class="col-12 col-xl-8">
<?php echo do_shortcode('[put_wpgm id=7]'); ?>
                    </div>

                    <div class="col-sm-12 col-xl-4">
                        <aside class="office-aside">
                            <h5 class="text-center mb-4">
                            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Offices in your area:
						    	<?php } else { ?>
						    		Офисы в вашем регионе:
						    	<?php } ?>
                            </h5>
                            <?php echo do_shortcode('[sp_easyaccordion id="455"]'); ?>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-content-6" role="tabpanel" aria-labelledby="tab-6">
                <div class="row">
                    <div class="col-12 col-xl-8">
<?php echo do_shortcode('[put_wpgm id=8]'); ?>
                    </div>

                    <div class="col-sm-12 col-xl-4">
                        <aside class="office-aside">
                            <h5 class="text-center mb-4">
                            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Offices in your area:
						    	<?php } else { ?>
						    		Офисы в вашем регионе:
						    	<?php } ?>
                            </h5>
                            <?php echo do_shortcode('[sp_easyaccordion id="456"]'); ?>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-content-7" role="tabpanel" aria-labelledby="tab-7">
                <div class="row">
                    <div class="col-12 col-xl-8">
<?php echo do_shortcode('[put_wpgm id=9]'); ?>
                    </div>

                    <div class="col-sm-12 col-xl-4">
                        <aside class="office-aside">
                            <h5 class="text-center mb-4">
                            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
							    	Offices in your area:
						    	<?php } else { ?>
						    		Офисы в вашем регионе:
						    	<?php } ?>
                            </h5>
							<?php echo do_shortcode('[sp_easyaccordion id="457"]'); ?>
                        </aside>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>


<div class="container mt-5">
    <div class="question-container">
        <h4 class="text-center">Остались <span class="text__red-dark text-uppercase">ВОПРОСЫ?</span></h4>
        <p class="text-center">Вы можете оставить заявку на получение консультации от менеджера продаж, используя данную форму</p>
        <?php echo do_shortcode('[contact-form-7 id="273" title="Контактная форма 1"]'); ?>
    </div>
</div>


<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<h2>Partners</h2>
    	<?php } else { ?>
    		<h2>Наши партнеры</h2>
    	<?php } ?>
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>
	
	
</main>
<?php get_footer(); ?>