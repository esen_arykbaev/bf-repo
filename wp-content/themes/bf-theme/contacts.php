<?php
/**
 * Template Name: Шаблон страницы контактов
 */
?>
<?php get_header(); ?>
<main id="main" class="site-main" role="main">
	
<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php the_title(); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

            <div class="banner-menu">
            	<?php 
					$link = get_field('ssylka_1');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_2');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_3');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
            </div>

        </div>
    </div>
</section>


<section class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xl-8">
                <?php the_content(); ?>
            </div>

            <div class="col-sm-12 col-xl-4">

<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<aside class="faq-aside">
                    <h5 class="text-center">FAQ</h5>
                    <p>
                        Getting a loan is a responsible step that requires certain knowledge and a serious attitude.
                        Often, clients sign loan agreements without understanding the details, receive money, and only when the payment day approaches
                        - there are a lot of questions. A person becomes a hostage to his own inattention. To save you from misunderstandings
                        When communicating with our credit company, we present to your attention the answers to frequently asked questions from our customers:
                    </p>
                    <div class="faq-sidebar mt-5">
                        <a href="/chavo" class="d-block mt-2">How do I get a loan?</a>
                        <a href="/chavo" class="d-block mt-2">What language can I get information in?</a>
                        <a href="/chavo" class="d-block mt-2">What happens if I'm late on my loan payment?</a>
                        <a href="/chavo" class="d-block mt-2">I already have a loan but would like to take more. Is this possible?</a>
                        <a href="/chavo" class="d-block mt-2">What is a credit report and how can it affect me?</a>
                        <a href="/chavo" class="d-block mt-2">A relative asks me to speak</a>

                        <a href="/chavo" class="button-primary mt-5">View all questions</a>
                    </div>
                </aside>
    	<?php } else { ?>
    		<aside class="faq-aside">
                <h5 class="text-center">Часто задаваемые вопросы</h5>
                <p>
                    Получение кредита – это ответственный шаг, требующий определенных знаний и серьезного отношения.
                    Часто клиенты подписывают кредитные договоры, не разобравшись в деталях, получают деньги, и только когда подходит день выплат
                    - возникает масса вопросов. Человек становится заложником собственной невнимательности. Чтобы избавить Вас от недоразумений
                    при общении с нашей кредитной компанией, представляем вашему вниманию ответы на часто возникающие у наших клиентов вопросы:
                </p>
                <div class="faq-sidebar mt-5">
                    <a href="/chavo" class="d-block mt-2">Как мне получить кредит?</a>
                    <a href="/chavo" class="d-block mt-2">На каком языке я могу получить информацию?</a>
                    <a href="/chavo" class="d-block mt-2">Что будет если я просрочил платеж по кредиту?</a>
                    <a href="/chavo" class="d-block mt-2">У меня уже есть кредит, но я хотел бы взять ещё. Это возможно?</a>
                    <a href="/chavo" class="d-block mt-2">Что такое кредитная история и как она может повлиять на меня?</a>
                    <a href="/chavo" class="d-block mt-2">Родственник просит меня выступить у него</a>

                    <a href="/chavo" class="button-primary mt-5">Ознакомиться со всеми вопросами</a>
                </div>
            </aside>
    	<?php } ?>
    	
    	
                

            </div>
        </div>
    </div>

</section>


<div class="container mt-5">
    <a class="dg-widget-link" href="http://2gis.kg/bishkek/firm/70000001035957778/center/74.60579395294191,42.857406058749554/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Бишкека</a>
    <div class="dg-widget-link"><a href="http://2gis.kg/bishkek/center/74.605784,42.857214/zoom/16/routeTab/rsType/bus/to/74.605784,42.857214╎Байлык Финанс, микрокредитная компания?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Байлык Финанс, микрокредитная компания</a></div>
    <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
    <script charset="utf-8">new DGWidgetLoader({"width":'100%',"height":600,"borderColor":"#a3a3a3","pos":{"lat":42.857406058749554,"lon":74.60579395294191,"zoom":16},"opt":{"city":"bishkek"},"org":[{"id":"70000001035957778"}]});</script>
    <noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
</div>

<div class="container mt-5">
    <div class="question-container">
        <h4 class="text-center">Остались <span class="text__red-dark text-uppercase">ВОПРОСЫ?</span></h4>
        <p class="text-center">Вы можете оставить заявку на получение консультации от менеджера продаж, используя данную форму</p>
        <?php echo do_shortcode('[contact-form-7 id="273" title="Контактная форма 1"]'); ?>
    </div>
</div>


<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<h2>Partners</h2>
    	<?php } else { ?>
    		<h2>Наши партнеры</h2>
    	<?php } ?>
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>
	
	
</main>
<?php get_footer(); ?>