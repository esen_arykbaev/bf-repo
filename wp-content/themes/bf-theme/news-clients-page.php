<?php 
/**
 * Template Name: Шаблон страницы клиентов
 */

get_header(); ?>
<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php wp_title("", true); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

            <div class="banner-menu">
            	<?php 
					$link = get_field('ssylka_1');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_2');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_3');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
            </div>

        </div>
    </div>
</section>

<section class="news-section">
    <div class="container">

        <div class="row">
            <div class="col-12 col-md-9 m-0 p-0">
                <div class="row mb-5">
                	<?php $params = array('posts_per_page' => 9, 'cat' => array(5) ); 
            	    if (have_posts()) : query_posts($params); ?>
					<?php while (have_posts()) : the_post(); ?>
                    <div class="col-12 col-sm-6 col-md-4">
                        <a href="<?php the_permalink(); ?>" class="news-block">
                        	<?php if ( function_exists( 'add_theme_support' ) )
								the_post_thumbnail( array(370,9999), array('class' => 'news-block__img') ); 
							?>
                            <div class="news-block__date"><?php echo get_the_date('d.m.Y'); ?></div>
                            <p class="news-block__text"><?php the_title(); ?></p>
                        </a>
                    </div>
                    <?php endwhile; ?>
		               <div class="row text-center d-none" >
						   <div class="col-xs-12">
						  	<?php the_posts_pagination( array (
										'mid_size' => 5,
										'prev_next'    => true, 
										'prev_text'    => __('«'),
										'next_text'    => __('»'),
										'type'         => 'list',
									)); ?>
						   </div>
						</div>
	    			<?php else : ?>
					<?php endif; wp_reset_query(); ?>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h3 class="news-archive__title">Архив новостей</h3>
                    </div>
                    <div class="col-12">
                        <a href="#" class="news-archive w-100">
                            <span class="news-archive__date">20.12.2021</span>
                            <span class="news-archive__info">
                                19 ноября 2021 года в Национальной библиотеке Кыргызской Республики им.А. Осмонова в г.Бишкек состоялся...
                            </span>
                        </a>
                        <a href="#" class="news-archive w-100">
                            <span class="news-archive__date">20.12.2021</span>
                            <span class="news-archive__info">
                                Национальный банк Кыргызской Республики принял решение о согласовании кандидатуры Балиашвили Вано...
                            </span>
                        </a>
                        <a href="#" class="news-archive w-100">
                            <span class="news-archive__date">20.12.2021</span>
                            <span class="news-archive__info">
                                Программа "Молодые профессионалы"
                            </span>
                        </a>
                        <a href="#" class="news-archive w-100">
                            <span class="news-archive__date">20.12.2021</span>
                            <span class="news-archive__info">
                                19 ноября 2021 года в Национальной библиотеке Кыргызской Республики им.А. Осмонова в г.Бишкек состоялся...
                            </span>
                        </a>
                        <a href="#" class="news-archive w-100">
                            <span class="news-archive__date">20.12.2021</span>
                            <span class="news-archive__info">
                                119 ноября 2021 года в Национальной библиотеке Кыргызской Республики им.А. Осмонова в г.Бишкек состоялся...
                            </span>
                        </a>
                        <a href="#" class="news-archive w-100">
                            <span class="news-archive__date">20.12.2021</span>
                            <span class="news-archive__info">
                                Национальный банк Кыргызской Республики принял решение о согласовании кандидатуры Балиашвили Вано...
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
            	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
					<?php dynamic_sidebar( 'sidebar-1' ); ?>
				<?php endif; ?>
            </div>
        </div>

    </div>
</section>
<?php get_footer(); ?>