<?php get_header(); ?>
<section 
class="banner" 
style="background-image: url('<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1><?php wp_title("", true); ?></h1>
	            <p>
	            	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	            	Be aware of all events
	            	<?php } else { ?>
	            	Будь в курсе всех событий
	            	<?php } ?>
	            </p>
            </div>

			<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
		    	<div class="banner-menu">
	            	<a class="d-flex align-items-center" href="/en/o-kompanii/">
				    	About us
				    </a>
				    <a class="d-flex align-items-center" href="/en/news/">
				    	News
				    </a>
				    <a class="d-flex align-items-center" href="/en/chavo/">
				    	FAQ
				    </a>
	            </div>
	    	<?php } else { ?>
	    		<div class="banner-menu">
	            	<a class="d-flex align-items-center" href="/o-kompanii/">
				    	О компании
				    </a>
				    <a class="d-flex align-items-center" href="/news/">
				    	Новости
				    </a>
				    <a class="d-flex align-items-center" href="/chavo/">
				    	ЧАВО
				    </a>
	            </div>
	    	<?php } ?>
            

        </div>
    </div>
</section>

<section class="news-section">
    <div class="container">
    		<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	            <h2 class="text-center h2">Search</h2>
				<h4 class="text-center"><b>Search results: <span style="color:red;"><?php the_search_query(); ?></span></b></h4>
        	<?php } else { ?>
        		<h2 class="text-center h2">Поиск</h2>
				<h4 class="text-center"><b>Результаты поиска: <span style="color:red;"><?php the_search_query(); ?></span></b></h4>
        	<?php } ?>
	            	
				
        <div class="row">
            <div class="col-12">
            	
                <div class="row mb-5">
                	<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
                    <div class="col-12">
                        <a href="<?php the_permalink(); ?>" class="news-block d-none">
                        	<?php if ( function_exists( 'add_theme_support' ) )
								the_post_thumbnail( array(370,9999), array('class' => 'news-block__img') ); 
							?>
                            <div class="news-block__date"><?php echo get_the_date('d.m.Y'); ?></div>
                            <p class="news-block__text"><?php the_title(); ?></p>
                        </a>
                        <div class="news-archive w-100">
                        	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
			            		<a href="/en/<?php echo get_the_date('Y'); ?>" class="news-archive__date text-decoration-none">
	                            	<?php echo get_the_date('d.m.Y'); ?>
	                            </a>
				        	<?php } else { ?>
				        		<a href="/<?php echo get_the_date('Y'); ?>" class="news-archive__date text-decoration-none">
	                            	<?php echo get_the_date('d.m.Y'); ?>
	                            </a>
				        	<?php } ?>
                            
                            <a href="<?php the_permalink(); ?>" class="news-archive__info text-decoration-none">
                                <?php the_title(); ?>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
		               <div class="row text-center d-none" >
						   <div class="col-xs-12">
						  	<?php the_posts_pagination( array (
										'mid_size' => 5,
										'prev_next'    => true, 
										'prev_text'    => __('«'),
										'next_text'    => __('»'),
										'type'         => 'list',
									)); ?>
						   </div>
						</div>
	    			<?php else : ?>
					<?php endif; wp_reset_query(); ?>
                </div>
            </div>
        </div>

    </div>
</section>
<?php get_footer(); ?>



