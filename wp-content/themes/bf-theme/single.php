<?php
/**
 * The template for displaying all single posts and attachments
 */
get_header(); ?>
<section 
class="banner" 
style="background-image: url('https://bf.startup.kg/wp-content/uploads/2022/01/news-bg.jpg')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	            	News
	            	<?php } else { ?>
	            	Новости
	            	<?php } ?>
                </h1>
                <p>
                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	            	Be aware of all events
	            	<?php } else { ?>
	            	Будь в курсе всех событий
	            	<?php } ?>
                </p>
            </div>

            <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
		    	<div class="banner-menu">
	            	<a class="d-flex align-items-center" href="/en/o-kompanii/">
				    	About us
				    </a>
				    <a class="d-flex align-items-center" href="/en/news/">
				    	News
				    </a>
				    <a class="d-flex align-items-center" href="/en/chavo/">
				    	FAQ
				    </a>
	            </div>
	    	<?php } else { ?>
	    		<div class="banner-menu">
	            	<a class="d-flex align-items-center" href="/o-kompanii/">
				    	О компании
				    </a>
				    <a class="d-flex align-items-center" href="/news/">
				    	Новости
				    </a>
				    <a class="d-flex align-items-center" href="/chavo/">
				    	ЧАВО
				    </a>
	            </div>
	    	<?php } ?>

        </div>
    </div>
</section>

<article class="news-section news-single">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <div class="row news-single__content">
                	<?php
						while ( have_posts() ) : the_post();
							get_template_part( 'content' );
						endwhile;
					?>
                </div>
				<div class="row">
					<div class="col-12"  style="margin: 20px 0;">
						<ul class="d-flex mb-4 p-0 list-none list-style-none" style="list-style: none">
							<li class="node-author mr-2">
								<span class="fa fa-fw fa-user"></span> 
								<span class="username">admin &nbsp; &nbsp; &nbsp;</span>
							</li>
							<li class="node-time mr-2">
								<time datetime="<?php the_date('d / M / Y'); ?>">
									<span class="fa fa-fw fa-calendar"></span> <?php echo get_the_date(); ?>  &nbsp; &nbsp; &nbsp;						
								</time>
							</li>
							<li class="node-views">
								<span class="fa fa-fw fa-eye"></span> <?php echo do_shortcode('[WPPV-TOTAL-VIEWS]') ?>  
								<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
			            	views
			            	<?php } else { ?>
			            	просм.
			            	<?php } ?>
								
							</li>
						</ul>
						
						<script src="https://yastatic.net/share2/share.js"></script>
						<div class="ya-share2" data-curtain data-services="vkontakte,facebook,odnoklassniki,telegram,twitter,viber,whatsapp,linkedin"></div>
					</div>
				</div>

            </div>

            <div class="col-sm-12 col-md-4">

                <div class="news-scroll">

                    <section class="news-container__list">
                    	
                        <h5>
                        	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
			            	Read also
			            	<?php } else { ?>
			            	Читайте также
			            	<?php } ?>
                        </h5> 
                        	
                        	<?php $post_id = get_the_ID(); ?>
                        
                        <?php $params = array('posts_per_page' => 20, 'cat' => array(1), 'post__not_in' => array($post_id) ); 
	            	    if (have_posts()) : query_posts($params); ?>
						<?php while (have_posts()) : the_post(); ?>
	                    <div class="d-flex align-items-center mb-2">
                            <div class="news-container__date d-flex align-items-center justify-content-center flex-column">
                                <span class="news-container__date-count"><?php echo get_the_date('d'); ?></span>
                                <span class="news-container__date-month text__red"><?php echo get_the_date('M'); ?></span>
                            </div>
                            <div class="news-container__info position-relative w-100">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?>.</a>
                            </div>
                        </div>
	                    <?php endwhile; ?>
						<?php endif; wp_reset_query(); ?>
                    </section>

                </div>
            </div>
        </div>
    </div>
</article>






<?php get_footer(); ?>
