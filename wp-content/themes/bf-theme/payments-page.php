<?php
/**
 * Template Name: Шаблон страницы оплат
 */

get_header(); ?>

<main id="main" class="site-main" role="main">

<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php the_title(); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

            <div class="banner-menu">
            	<?php 
					$link = get_field('ssylka_1');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_2');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_3');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
            </div>

        </div>
    </div>
</section>

<section class="credits">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 p-md-0 m-md-0">
                <div class="credits-scroll">
                    <ul class="nav nav-tabs credits-tabs" id="myTab" role="tablist">
                    	<?php if( have_rows('tab_1') ): ?>
						    <?php while( have_rows('tab_1') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tab-content-1" type="button" role="tab" aria-controls="tab-content-1" aria-selected="true">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
                        <?php if( have_rows('tab_2') ): ?>
						    <?php while( have_rows('tab_2') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-2" data-bs-toggle="tab" data-bs-target="#tab-content-2" type="button" role="tab" aria-controls="tab-content-2" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_3') ): ?>
						    <?php while( have_rows('tab_3') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-3" data-bs-toggle="tab" data-bs-target="#tab-content-3" type="button" role="tab" aria-controls="tab-content-3" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_4') ): ?>
						    <?php while( have_rows('tab_4') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-4" data-bs-toggle="tab" data-bs-target="#tab-content-4" type="button" role="tab" aria-controls="tab-content-4" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_5') ): ?>
						    <?php while( have_rows('tab_5') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-5" data-bs-toggle="tab" data-bs-target="#tab-content-5" type="button" role="tab" aria-controls="tab-content-6" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
					    <?php if( have_rows('tab_6') ): ?>
						    <?php while( have_rows('tab_6') ): the_row(); ?>
							    <?php if( get_sub_field('zagolovok') ) { ?>
					                <li class="nav-item" role="presentation">
			                            <button class="nav-link " id="tab-6" data-bs-toggle="tab" data-bs-target="#tab-content-6" type="button" role="tab" aria-controls="tab-content-6" aria-selected="false">
			                            	<?php the_sub_field('zagolovok'); ?>
			                            </button>
			                        </li>
							    <?php } ?>
						    <?php endwhile; ?>
					    <?php endif; ?>
                    </ul>
                </div>
                <div class="tab-content credits-tabs-content" id="myTabContent">
                	
                	<?php if( have_rows('tab_1') ): ?>
					    <?php while( have_rows('tab_1') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">
			                        
			                        <div class="credits-table">
			                            <p><?php the_sub_field('opisanie'); ?></p>
								
								        <div class="payments-container d-flex flex-wrap">
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-1.jpg" alt="">
								                <h6>PayWay</h6>
								                <span>0 %</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-2.jpg" alt="">
								                <h6>РСК</h6>
								                <span>50 сом</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-3.jpg" alt="">
								                <h6>Umai</h6>
								                <span>от 10 до 50 сомов</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-4.jpg" alt="">
								                <h6>Finca bank</h6>
								                <span>от 10 до 20 сомов</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-5.jpg" alt="">
								                <h6>Оной</h6>
								                <span>от 5 до 20 сом</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-6.jpg" alt="">
								                <h6>Quickpay</h6>
								                <span>2.5% от суммы погашения</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-7.jpg" alt="">
								                <h6>Pay24</h6>
								                <span>3% от суммы погашения</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-8.jpg" alt="">
								                <h6>О!</h6>
								                <span>0,4 % от суммы погашения</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-9.jpg" alt="">
								                <h6>Керемет Банк</h6>
								                <span>0,3% от суммы погашения</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-10.jpg" alt="">
								                <h6>Dos Credo Bank</h6>
								                <span>20 сомов</span>
								            </div>
								
								        </div>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_2') ): ?>
					    <?php while( have_rows('tab_2') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
			                        
			                        <div class="credits-table">
			                            <p><?php the_sub_field('opisanie'); ?></p>
			                            
			                            <div class="payments-container payments-container__second d-flex flex-wrap">

								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-11.jpg" alt="">
								                <h6>Mbank АКБ</h6>
								                <span>10 сом</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-12.jpg" alt="">
								                <h6>Balance</h6>
								                <span>0,5 % от суммы погашения</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-13.jpg" alt="">
								                <h6>Элсом</h6>
								                <span>0 сом</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-14.jpg" alt="">
								                <h6>О! Деньги</h6>
								                <span>0 сом</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-15.jpg" alt="">
								                <h6>Элкарт мобайл</h6>
								                <span>0 сом</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-16.jpg" alt="">
								                <h6>Компаньон</h6>
								                <span>0 сом</span>
								            </div>
								
								            <div class="payments-block">
								                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/pay/p-17.jpg" alt="">
								                <h6>RSK 24</h6>
								                <span>50 сом</span>
								            </div>
								
								        </div>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_3') ): ?>
					    <?php while( have_rows('tab_3') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fadee" id="tab-content-3" role="tabpanel" aria-labelledby="tab-3">
			                        <div class="credits-table">
			                            <p><?php the_sub_field('opisanie'); ?></p>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_4') ): ?>
					    <?php while( have_rows('tab_4') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-4" role="tabpanel" aria-labelledby="tab-4">
			                        <div class="credits-table">
			                            <p><?php the_sub_field('opisanie'); ?></p>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_5') ): ?>
					    <?php while( have_rows('tab_5') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-5" role="tabpanel" aria-labelledby="tab-5">
			                        <div class="credits-table">
			                            <p><?php the_sub_field('opisanie'); ?></p>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
				    
				    <?php if( have_rows('tab_6') ): ?>
					    <?php while( have_rows('tab_6') ): the_row(); ?>
						    <?php if( get_sub_field('zagolovok') ) { ?>
				                <div class="tab-pane fade" id="tab-content-6" role="tabpanel" aria-labelledby="tab-6">
			                       <div class="credits-table">
			                            <p><?php the_sub_field('opisanie'); ?></p>
			                        </div>
			                    </div>
						    <?php } ?>
					    <?php endwhile; ?>
				    <?php endif; ?>
                	
                	
                	
                    
                </div>
            </div>
            
        </div>
    </div>

</section>

<section class="payments p-0">
    <div class="container">
      <?php the_content(); ?>
    </div>
</section>


<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<h2>Partners</h2>
    	<?php } else { ?>
    		<h2>Наши партнеры</h2>
    	<?php } ?>
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>

</main><!-- .site-main -->

<?php get_footer(); ?>
