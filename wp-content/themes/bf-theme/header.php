<!doctype html>
<html lang="en">
<head>
    <title><?php wp_title( '|', true, 'right' ); ?> Байлык Финанс</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="format-detection" content="telephone=no">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo/icon-32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo/icon-16.png">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@3.6.9/dist/css/splide.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/overlayscrollbars/1.13.1/css/OverlayScrollbars.min.css"
          integrity="sha512-jN4O0AUkRmE6Jwc8la2I5iBmS+tCDcfUd1eq8nrZIBnDKTmCp5YxxNN1/aetnAH32qT+dDbk1aGhhoaw5cJNlw=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.1/slimselect.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.min.css">

    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/css/styles.css?v=<?php echo time(); ?>">
    
    <style>
    	header .menu-social a {
    		margin-right: 10px;
    	}
    	#menu-header-menu {
    		background: rgba(255,255,255, 0.7);
    		border-radius: 10px;
    	}
    	
    </style>
    
    <?php wp_head(); ?>
</head>
<body <?php body_class() ?>>


<header class="header w-100">
    <div class="container d-flex flex-wrap justify-content-center">
    	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<a href="/en/" class="logo-link d-flex align-items-center text-decoration-none me-xl-auto">
	        	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo/new-logo-eng.svg" alt="" style="max-width: 190px;">
	        </a>
    	<?php } else { ?>
    		<a href="/" class="logo-link d-flex align-items-center text-decoration-none me-xl-auto">
	        	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo/new-logo.svg" alt="" style="max-width: 190px;">
	        </a>
    	<?php } ?>
    	
        <div class="d-flex flex-column">
        	
            <div class="header-top position-relative  d-flex justify-content-between">
            	<div class="align-items-center justify-content-between menu-social d-none d-xxl-flex">
	                <a href="https://t.me/BailykFinance_bot" target="_blank" class="text-decoration-none">
	                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
	                        <path fill="#293845" fill-rule="evenodd" clip-rule="evenodd" d="M0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0C7.16344 0 0 7.16344 0 16ZM13.0667 23.3333L13.3389 19.2548L13.3387 19.2547L20.758 12.5593C21.0836 12.2704 20.6869 12.1294 20.2546 12.3916L11.0982 18.1682L7.14314 16.9338C6.28902 16.6723 6.28289 16.0853 7.3349 15.6634L22.7469 9.72058C23.4508 9.401 24.1302 9.88964 23.8615 10.967L21.2368 23.3354C21.0535 24.2143 20.5225 24.4245 19.7867 24.0185L15.7885 21.0646L13.8667 22.9333C13.8606 22.9392 13.8546 22.9451 13.8486 22.9509C13.6337 23.1602 13.4558 23.3333 13.0667 23.3333Z" />
	                    </svg>
	                </a>
	                <a href="https://api.whatsapp.com/send/?phone=996701511761&text&app_absent=0" target="_blank" class="text-decoration-none">
	                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0C7.16344 0 0 7.16344 0 16ZM26.0415 15.3877C26.0394 20.6322 21.773 24.8995 16.5274 24.9017H16.5235C14.9312 24.9011 13.3666 24.5016 11.9769 23.7436L6.93337 25.0667L8.28311 20.1365C7.45052 18.6937 7.01242 17.057 7.01313 15.3801C7.01522 10.1344 11.2832 5.86665 16.5273 5.86665C19.0725 5.86775 21.4612 6.85856 23.2575 8.65692C25.0538 10.4552 26.0425 12.8455 26.0415 15.3877Z" fill="#293845"/>
	                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.21783 22.8064L12.2108 22.0213L12.4995 22.1926C13.7136 22.9131 15.1054 23.2944 16.5244 23.2949H16.5276C20.8861 23.2949 24.4335 19.7474 24.4352 15.3871C24.436 13.2741 23.6143 11.2874 22.1213 9.79267C20.6283 8.29794 18.6428 7.47437 16.5307 7.47363C12.1688 7.47363 8.62136 11.0207 8.61963 15.3807C8.61902 16.8749 9.03709 18.3301 9.82867 19.5891L10.0167 19.8883L9.21783 22.8064ZM20.9951 17.4361C21.1609 17.5162 21.2729 17.5703 21.3207 17.6501C21.3801 17.7492 21.3801 18.2252 21.1821 18.7806C20.9839 19.3359 20.0342 19.8427 19.5774 19.9109C19.1679 19.9721 18.6496 19.9977 18.0801 19.8167C17.7349 19.7072 17.2921 19.5609 16.7249 19.3159C14.4963 18.3536 12.9902 16.1936 12.7055 15.7854C12.6856 15.7568 12.6717 15.7368 12.6639 15.7264L12.662 15.7238C12.5362 15.556 11.6932 14.4312 11.6932 13.2672C11.6932 12.1722 12.2311 11.5982 12.4787 11.334C12.4956 11.3159 12.5112 11.2993 12.5252 11.284C12.7431 11.046 13.0007 10.9865 13.1591 10.9865C13.3176 10.9865 13.4763 10.988 13.6148 10.9949C13.6318 10.9958 13.6496 10.9957 13.668 10.9956C13.8065 10.9948 13.9792 10.9938 14.1496 11.403C14.2152 11.5605 14.3111 11.794 14.4122 12.0403C14.6168 12.5383 14.8428 13.0885 14.8826 13.1682C14.942 13.2872 14.9816 13.4259 14.9024 13.5847C14.8905 13.6085 14.8795 13.6309 14.869 13.6524C14.8095 13.7739 14.7657 13.8633 14.6647 13.9812C14.625 14.0276 14.5839 14.0776 14.5429 14.1276C14.4611 14.2272 14.3793 14.3268 14.3081 14.3977C14.1891 14.5162 14.0652 14.6449 14.2039 14.8829C14.3425 15.1209 14.8196 15.8993 15.5263 16.5297C16.286 17.2073 16.9463 17.4937 17.281 17.6388C17.3463 17.6672 17.3993 17.6902 17.4381 17.7096C17.6757 17.8286 17.8144 17.8087 17.9531 17.6501C18.0918 17.4915 18.5474 16.9559 18.7058 16.718C18.8643 16.4801 19.0228 16.5197 19.2407 16.599C19.4586 16.6784 20.6274 17.2534 20.8651 17.3724C20.9115 17.3957 20.9549 17.4166 20.9951 17.4361Z" fill="#293845"/>
	                    </svg>
	                </a>
	                <a href="https://www.instagram.com/bailykfinance.kg/" target="_blank" class="text-decoration-none">
	                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" clip-rule="evenodd" d="M16 0C7.16344 0 0 7.16344 0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0ZM12.4822 7.51824C13.3924 7.47682 13.6833 7.46668 16.0008 7.46668H15.9982C18.3164 7.46668 18.6062 7.47682 19.5164 7.51824C20.4249 7.55984 21.0453 7.70366 21.5893 7.91469C22.1511 8.13247 22.6258 8.42403 23.1004 8.8987C23.5751 9.37301 23.8667 9.84911 24.0853 10.4104C24.2951 10.9529 24.4391 11.573 24.4818 12.4815C24.5227 13.3917 24.5334 13.6826 24.5334 16.0001C24.5334 18.3176 24.5227 18.6078 24.4818 19.518C24.4391 20.4261 24.2951 21.0464 24.0853 21.5891C23.8667 22.1502 23.5751 22.6263 23.1004 23.1006C22.6263 23.5753 22.1509 23.8676 21.5899 24.0855C21.0469 24.2965 20.4261 24.4404 19.5177 24.482C18.6074 24.5234 18.3175 24.5335 15.9998 24.5335C13.6824 24.5335 13.3917 24.5234 12.4815 24.482C11.5732 24.4404 10.9529 24.2965 10.41 24.0855C9.84911 23.8676 9.37301 23.5753 8.89888 23.1006C8.42438 22.6263 8.13282 22.1502 7.91469 21.589C7.70384 21.0464 7.56002 20.4263 7.51824 19.5178C7.47699 18.6076 7.46668 18.3176 7.46668 16.0001C7.46668 13.6826 7.47735 13.3915 7.51806 12.4813C7.55895 11.5732 7.70295 10.9529 7.91451 10.4102C8.13318 9.84911 8.42474 9.37301 8.89941 8.8987C9.37372 8.4242 9.84982 8.13265 10.4111 7.91469C10.9537 7.70366 11.5737 7.55984 12.4822 7.51824Z" fill="#293845"/>
	                        <path fill-rule="evenodd" clip-rule="evenodd" d="M15.2355 9.00445C15.3841 9.00422 15.544 9.00429 15.7166 9.00437L16.001 9.00445C18.2794 9.00445 18.5495 9.01263 19.4492 9.05352C20.2812 9.09156 20.7328 9.23059 21.0336 9.34739C21.4318 9.50206 21.7157 9.68695 22.0142 9.98562C22.3129 10.2843 22.4978 10.5687 22.6528 10.967C22.7696 11.2674 22.9088 11.719 22.9467 12.551C22.9876 13.4505 22.9964 13.7208 22.9964 15.9981C22.9964 18.2755 22.9876 18.5457 22.9467 19.4453C22.9086 20.2773 22.7696 20.7288 22.6528 21.0293C22.4981 21.4275 22.3129 21.7111 22.0142 22.0096C21.7155 22.3082 21.432 22.4931 21.0336 22.6478C20.7331 22.7651 20.2812 22.9038 19.4492 22.9418C18.5496 22.9827 18.2794 22.9916 16.001 22.9916C13.7224 22.9916 13.4524 22.9827 12.5528 22.9418C11.7208 22.9034 11.2692 22.7644 10.9682 22.6476C10.57 22.4929 10.2856 22.3081 9.98689 22.0094C9.68822 21.7107 9.50333 21.427 9.34831 21.0286C9.23151 20.7281 9.09231 20.2766 9.05444 19.4446C9.01355 18.545 9.00537 18.2748 9.00537 15.996C9.00537 13.7172 9.01355 13.4484 9.05444 12.5488C9.09248 11.7168 9.23151 11.2653 9.34831 10.9645C9.50298 10.5662 9.68822 10.2818 9.98689 9.98313C10.2856 9.68446 10.57 9.49957 10.9682 9.34455C11.269 9.22721 11.7208 9.08854 12.5528 9.05032C13.34 9.01476 13.6451 9.0041 15.2355 9.00232V9.00445ZM20.5561 10.4214C19.9907 10.4214 19.5321 10.8795 19.5321 11.445C19.5321 12.0104 19.9907 12.4691 20.5561 12.4691C21.1214 12.4691 21.5801 12.0104 21.5801 11.445C21.5801 10.8797 21.1214 10.421 20.5561 10.421V10.4214ZM11.6188 16C11.6188 13.58 13.5808 11.6179 16.0009 11.6178C18.421 11.6178 20.3826 13.5799 20.3826 16C20.3826 18.4202 18.4212 20.3814 16.001 20.3814C13.5809 20.3814 11.6188 18.4202 11.6188 16Z" fill="#293845"/>
	                        <path d="M16.0007 13.1556C17.5716 13.1556 18.8452 14.4291 18.8452 16.0001C18.8452 17.571 17.5716 18.8446 16.0007 18.8446C14.4297 18.8446 13.1562 17.571 13.1562 16.0001C13.1562 14.4291 14.4297 13.1556 16.0007 13.1556Z" fill="#293845"/>
	                    </svg>
	                </a>
	                <a href="https://www.facebook.com/www.bf.kg/" target="_blank" class="text-decoration-none">
	                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" clip-rule="evenodd" d="M16 0C7.16344 0 0 7.16344 0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0ZM17.6677 16.7028V25.4077H14.0661V16.7031H12.2667V13.7034H14.0661V11.9024C14.0661 9.4552 15.0821 8 17.9688 8H20.3721V11.0001H18.8699C17.7462 11.0001 17.6718 11.4193 17.6718 12.2017L17.6677 13.7031H20.3891L20.0707 16.7028H17.6677Z" fill="#293845"/>
	                    </svg>
	                </a>
	                <a href="https://www.youtube.com/channel/UCEbNje1-On_srEowfUQ2bJw" target="_blank" class="text-decoration-none">
	                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" clip-rule="evenodd" d="M16 0C7.16344 0 0 7.16344 0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0ZM22.6677 10.4996C23.402 10.7011 23.9803 11.2948 24.1766 12.0488C24.5333 13.4154 24.5333 16.2667 24.5333 16.2667C24.5333 16.2667 24.5333 19.1179 24.1766 20.4845C23.9803 21.2385 23.402 21.8323 22.6677 22.0339C21.3369 22.4 15.9999 22.4 15.9999 22.4C15.9999 22.4 10.663 22.4 9.3321 22.0339C8.59775 21.8323 8.01943 21.2385 7.82316 20.4845C7.4666 19.1179 7.4666 16.2667 7.4666 16.2667C7.4666 16.2667 7.4666 13.4154 7.82316 12.0488C8.01943 11.2948 8.59775 10.7011 9.3321 10.4996C10.663 10.1333 15.9999 10.1333 15.9999 10.1333C15.9999 10.1333 21.3369 10.1333 22.6677 10.4996Z" fill="#293845"/>
	                        <path d="M14.3999 19.2V13.8667L18.6666 16.5335L14.3999 19.2Z" fill="#293845"/>
	                    </svg>
	                </a>
	            </div>
            
                <div class="header-top__block">
                    <select id="number-select">
                        <option value="+996559991111">+996 (559) 991 111</option>
                        <option value="+996220991111">+996 (220) 991 111</option>
                        <option value="+996509991111">+996 (509) 991 113</option>
                        <option value="+996701511761">+996 (701) 511 761</option>
                        <option value="+996312979444">+996 (312) 979 444</option>
                    </select>
                </div>

                <div class="header-top__block">
                	<?php get_search_form(); ?>
                </div>

                <div class="header-top__block">
                    <a href="<?php if ( WPGlobus::Config()->language == 'ru' ) { ?>/chavo<?php } else { ?>/en/chavo<?php } ?>" class="mx-4 d-flex align-items-center text-decoration-none ">
                    	
                    	<?php if ( WPGlobus::Config()->language == 'ru' ) { ?>
                    		ЧАВО
                    	<?php } else { ?>
                    	F.A.Q
                        <?php } ?>
                        <svg class="ms-2" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M13 2V9H3.17L2.58 9.59L2 10.17V2H13ZM14 0H1C0.45 0 0 0.45 0 1V15L4 11H14C14.55 11 15 10.55 15 10V1C15 0.45 14.55 0 14 0ZM19 4H17V13H4V15C4 15.55 4.45 16 5 16H16L20 20V5C20 4.45 19.55 4 19 4Z" fill="#293845"/>
                        </svg>
                    </a>
                </div>

                <div class="header-top__block">
                    <select id="lang-select">
                        <option value="ru">РУС</option>
                        <option value="en">ENG</option>
                        <!--<option value="kg">KG</option>-->
                    </select>
                    <div class="d-none">
                    	<?php the_widget( 'WPGlobusWidget' ); ?>
                    </div>
                </div>

            </div>

            <nav class="header-bottom d-flex align-items-center">
            	
            	<?php
					if ( function_exists( 'wp_nav_menu' ) )
						wp_nav_menu( 
							array( 
							'theme_location' => 'header-menu',
							'fallback_cb'=> 'header_menu',
							'container' => 'ul',
							'menu_class' => 'navbar-nav align-items-center justify-content-end ms-auto', 
							'menu_id' => '',
							'depth'             => 3,
							'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							  'walker' => new wp_bootstrap_navwalker()
							) 
						);
				?>
                
                <button class="hamburger d-flex align-items-center justify-content-around flex-column">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </nav>
        </div>
    </div>
</header>


