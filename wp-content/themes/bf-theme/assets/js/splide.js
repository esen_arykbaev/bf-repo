document.addEventListener( 'DOMContentLoaded', function() {

    if(document.querySelector('#partnersSlider')) {
        const partnersSlider = new Splide('#partnersSlider', {
            type    : 'loop',
            perPage : 10,
            gap: '20px',
            drag   : 'free',
            height   : '55px',
            rewind    : true,
            pagination: false,
            arrows: false,
            focus    : 'center',
            autoWidth: true,
            autoScroll: {
                speed: 1,
            },
        });
        partnersSlider.mount(window.splide.Extensions);
    }

    if(document.querySelector('#gallery-slider')) {
        const gallerySlider = new Splide('#gallery-slider', {
            type    : 'loop',
            perPage : 1,
            gap: '32px',
            drag   : 'free',
            rewind    : true,
            pagination: false,
            arrows: false,
            autoScroll: {
                speed: 1,
            },
        });
        gallerySlider.mount(window.splide.Extensions);
    }

});

