const numberFormat = new Intl.NumberFormat();
if ($("#slider_universal").length > 0) {
	
	// numberFormat.format(ui.values[0])
    $("#slider_universal").slider({
        range: "min",
        animate: true,
        min: 5000,
        max: 50000,
        step: 1,
        value: 50000,
        slide: function( event, ui ) {
            //  $( "#slider_universal-result span" ).html( ui.value );
            $( "#universal_hidden" ).val(ui.value);
        },
        change: function( event, ui ) {
            $( "#universal_hidden" ).val(ui.value);
            Calc(parseInt($('#initpay').text()))
        }
    });

    $("#slider_universal_min_payment").slider({
        range: "min",
        animate: true,
        min: 1,
        max: 18,
        step: 1,
        value: 12,
        slide: function( event, ui ) {
            // $( "#slider_universal_min_payment-result span" ).html( ui.value );
            $( "#slider_universal_min_payment-hidden" ).val(ui.value);
        },
        change: function( event, ui ) {
            $( "#slider_universal_min_payment-hidden" ).val(ui.value);
            $(".calc_result--btn").attr("data-month", ui.value);
            Calc(parseInt($('#initpay').text()))
        }
    });

    // $("#slider_procent").slider({
    //     range: "min",
    //     animate: true,
    //     min: 23,
    //     max: 30,
    //     step: 1,
    //     value: 28,
    //     slide: function( event, ui ) {
    //         $( "#initpay" ).text(`${ui.value} %`);
    //         $( "#slider_universal_min_payment-hidden2" ).val(ui.value);
    //     },
    //     change: function( event, ui ) {
    //         $( "#slider_universal_min_payment-hidden2" ).val(ui.value);
    //         $(".calc_result--btn").attr("data-procent", ui.value);
    //         Calc(ui.value)
    //     }
    // });

    $("#slider_universal_min_payment-hidden").change(function() {
        $("#slider_universal_min_payment").slider("value", $(this).val());
    });

    $("#universal_hidden").change(function() {
    	console.log('$(this).val()', $(this).val())
        $("#slider_universal").slider("value", $(this).val());
    });
    
    const percentageBlock = document.querySelector('.percentage-block');
    const unitPayBlock = document.querySelector('#initpay');
    
    if (document.querySelector('#selectCredit')) {
    	new SlimSelect({
            select: '#selectCredit',
            showSearch: false,
            onChange: (info) => {
            	console.log('info', info)
                $('.calc_result--btn').attr("data-procent", info.value);
                unitPayBlock.innerHTML = `${info.value} %`;
                percentageBlock.value = info.value;
                
                
                if (info.text === 'Улучшение жилья' || info.text === 'Home Improvement') {
                	// CHANGE SUMM
                	$("#slider_universal").slider("value", 50000);
                	$("#slider_universal").slider("option", "min", 10000);
                	$("#slider_universal").slider("option", "max", 300000);
                	
                	$("#slider_universal_min_term").text('10000'); // middle
                	$("#slider_universal-result").text('100000'); // min 
                	$("#slider_universal_max_term").text('300000'); // max
                	
                	$("#universal_hidden").attr({
					       "min" : 10000,
					       "max" : 300000,
					       "value": 50000
					 });
					 
					 
					 // CHANGE MONTH
					$("#slider_universal_min_payment").slider("value", 12);
                	$("#slider_universal_min_payment").slider("option", "min", 1);
                	$("#slider_universal_min_payment").slider("option", "max", 24);
                	
                	$("#slider_universal_min_first_count").text('1 '); // middle
                	$("#slider_universal_min_count-result").text('12 '); // min 
                	$("#slider_universal_max_first_count").text('24 '); // max
                	
                	$("#slider_universal_min_payment-hidden").attr({
					       "min" : 1,
					       "max" : 24,
					       "value": 12
					 });
                	Calc(info.value);
                }
                if (info.text === "Здоровье" || info.text === 'Health') {
                	// CHANGE SUMM
                	$("#slider_universal").slider("value", 50000);
                	$("#slider_universal").slider("option", "min", 5000);
                	$("#slider_universal").slider("option", "max", 50000);
                	
                	$("#slider_universal_min_term").text('5000'); // middle
                	$("#slider_universal-result").text('50000'); // min 
                	$("#slider_universal_max_term").text('50000'); // max
                	
                	$("#universal_hidden").attr({
					       "min" : 5000,
					       "max" : 50000,
					       "value": 50000
					 });
					 
					 
					 // CHANGE MONTH
					$("#slider_universal_min_payment").slider("value", 12);
                	$("#slider_universal_min_payment").slider("option", "min", 1);
                	$("#slider_universal_min_payment").slider("option", "max", 18);
                	
                	$("#slider_universal_min_first_count").text('1 '); // middle
                	$("#slider_universal_min_count-result").text('12 '); // min 
                	$("#slider_universal_max_first_count").text('18 '); // max
                	
                	$("#slider_universal_min_payment-hidden").attr({
					       "min" : 1,
					       "max" : 18,
					       "value": 12
					 });
                	Calc(info.value);
                }
                if (
                	info.text === "Агро кредит" || 
                	info.text === "Бизнес кредит" || 
                	info.text === 'Agro credit' || 
                	info.text === 'Business loan'
                ) {
                	// CHANGE SUMM
                	$("#slider_universal").slider("value", 50000);
                	$("#slider_universal").slider("option", "min", 10000);
                	$("#slider_universal").slider("option", "max", 1000000);
                	
                	$("#slider_universal_min_term").text('10000'); // middle
                	$("#slider_universal-result").text('500000'); // min 
                	$("#slider_universal_max_term").text('1000000'); // max
                	
                	$("#universal_hidden").attr({
					       "min" : 10000,
					       "max" : 1000000,
					       "value": 50000
					 });
					 
					 
					 // CHANGE MONTH
					$("#slider_universal_min_payment").slider("value", 12);
                	$("#slider_universal_min_payment").slider("option", "min", 1);
                	$("#slider_universal_min_payment").slider("option", "max", 36);
                	
                	$("#slider_universal_min_first_count").text('1 '); // middle
                	$("#slider_universal_min_count-result").text('12 '); // min 
                	$("#slider_universal_max_first_count").text('36 '); // max
                	
                	$("#slider_universal_min_payment-hidden").attr({
					       "min" : 1,
					       "max" : 36,
					       "value": 12
					 });
                	Calc(info.value);
                }
                
                if (
                	info.text === "Мурабаха" || 
                	info.text === "Миң түркүн" || 
                	info.text === "ВИЭ Кредит" ||
                	info.text === 'Murabaha' || 
                	info.text === 'Min turkun' || 
                	info.text === 'RES Credit'
                ) {
                	// CHANGE SUMM
                	$("#slider_universal").slider("value", 50000);
                	$("#slider_universal").slider("option", "min", 5000);
                	$("#slider_universal").slider("option", "max", 150000);
                	
                	$("#slider_universal_min_term").text('5000'); // middle
                	$("#slider_universal-result").text('100000'); // min 
                	$("#slider_universal_max_term").text('150000'); // max
                	
                	$("#universal_hidden").attr({
				       "min" : 5000,
				       "max" : 150000,
				       "value": 50000
					 });
					 
					 
					 // CHANGE MONTH
					$("#slider_universal_min_payment").slider("value", 12);
                	$("#slider_universal_min_payment").slider("option", "min", 1);
                	$("#slider_universal_min_payment").slider("option", "max", 18);
                	
                	$("#slider_universal_min_first_count").text('1 '); // middle
                	$("#slider_universal_min_count-result").text('12 '); // min 
                	$("#slider_universal_max_first_count").text('18 '); // max
                	
                	$("#slider_universal_min_payment-hidden").attr({
					       "min" : 1,
					       "max" : 18,
					       "value": 12
					 });
                	Calc(info.value);
                }
                
               
                if (info.text === "ЦДС Кредит" || info.text === 'CDS Credit') {
                	// CHANGE SUMM
                	$("#slider_universal").slider("value", 50000);
                	$("#slider_universal").slider("option", "min", 20000);
                	$("#slider_universal").slider("option", "max", 500000);
                	
                	$("#slider_universal_min_term").text('20000'); // middle
                	$("#slider_universal-result").text('100000'); // min 
                	$("#slider_universal_max_term").text('500000'); // max
                	
                	$("#universal_hidden").attr({
					       "min" : 20000,
					       "max" : 500000,
					       "value": 50000
					 });
					 
					 
					 // CHANGE MONTH
					$("#slider_universal_min_payment").slider("value", 12);
                	$("#slider_universal_min_payment").slider("option", "min", 3);
                	$("#slider_universal_min_payment").slider("option", "max", 24);
                	
                	$("#slider_universal_min_first_count").text('1 '); // middle
                	$("#slider_universal_min_count-result").text('12 '); // min 
                	$("#slider_universal_max_first_count").text('24 '); // max
                	
                	$("#slider_universal_min_payment-hidden").attr({
					       "min" : 3,
					       "max" : 24,
					       "value": 12
					 });
                	Calc(info.value);
                }
                
                if (info.text === "Потребительский" || info.text === 'Consumer') {
                	// CHANGE SUMM
                	$("#slider_universal").slider("value", 50000);
                	$("#slider_universal").slider("option", "min", 10000);
                	$("#slider_universal").slider("option", "max", 500000);
                	
                	$("#slider_universal_min_term").text('10000'); // middle
                	$("#slider_universal-result").text('100000'); // min 
                	$("#slider_universal_max_term").text('500000'); // max
                	
                	$("#universal_hidden").attr({
					       "min" : 10000,
					       "max" : 500000,
					       "value": 50000
					 });
					 
					 
					 // CHANGE MONTH
					$("#slider_universal_min_payment").slider("value", 12);
                	$("#slider_universal_min_payment").slider("option", "min", 1);
                	$("#slider_universal_min_payment").slider("option", "max", 36);
                	
                	$("#slider_universal_min_first_count").text('1 '); // middle
                	$("#slider_universal_min_count-result").text('12 '); // min 
                	$("#slider_universal_max_first_count").text('36 '); // max
                	
                	$("#slider_universal_min_payment-hidden").attr({
					       "min" : 1,
					       "max" : 36,
					       "value": 12
					 });
                	Calc(info.value);
                }
            }
        });	
    }
    
    percentageBlock.addEventListener('change', function(){
    	Calc(this.value);
    	unitPayBlock.innerHTML = `${this.value} %`;
    });

    function Calc(procent = 30) {

        const price = $('#price');
        let countCredit = parseInt($('#universal_hidden').val());
        let countDay = parseInt($('#slider_universal_min_payment-hidden').val());

        let one_month_percent = procent / 100 / 12;
        let monthly = countCredit * (one_month_percent + (one_month_percent / (Math.pow((1 + one_month_percent), countDay) - 1) ));
        const allSum = monthly * countDay;
        // let es_markup1 = allSum - countCredit;
        // let es_markup_floor1 = Math.floor(es_markup1);
        const monthly_payment_floor = Math.round(monthly);
        console.log('сумма кредита', Math.round(allSum));
        console.log('ежемесячный платеж', monthly_payment_floor);

        $('.calc_result--btn').attr("data-credit", Math.round(allSum));
        $('#allSum').text(`${numberFormat.format(Math.round(allSum))} `);
        $('#allDay').text(`${Math.round(countDay)} `);
        price.text(`${numberFormat.format(monthly_payment_floor)} `);
        
        console.log(Math.round(allSum))
        
        $("#myAnchor").attr("href", `/grafik-pogasheniya-kredita/?summ=${Math.round(allSum)}&srok=${Math.round(countDay)}&proc=${procent}`);

    }
    Calc();
}



