document.addEventListener("DOMContentLoaded", function() {
    //The first argument are the elements to which the plugin shall be initialized
    //The second argument has to be at least a empty object or a object with your desired options
    if (document.querySelector('.credits-scroll')) {
        OverlayScrollbars(document.querySelectorAll('.credits-scroll'), { });
    }
    if (document.querySelector('.thumbnails-scroll')) {
        OverlayScrollbars(document.querySelectorAll('.thumbnails-scroll'), { });
    }
    if (document.querySelector('.news-scroll')) {
        OverlayScrollbars(document.querySelectorAll('.news-scroll'), { });
    }
    if (document.querySelector('.scroll-block')) {
        OverlayScrollbars(document.querySelectorAll('.scroll-block'), { 	overflowBehavior: {
                "y": ["h"]
            } });
    }

    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });

    document.querySelectorAll('.footer-menu').forEach(menu => {
        menu.addEventListener('click', function (e) {
            this.classList.toggle('active');
        });
    });

    document.querySelectorAll('.main-menu__block').forEach(menu => {
        menu.addEventListener('click', function (e) {
            this.classList.toggle('active');
        });
    });

    const d = new Date();
    const date = d.getDate();
    const year = d.getFullYear();

    const months = [
        'Января',
        'Февраля',
        'Марта',
        'Апреля',
        'Мая',
        'Июня',
        'Июля',
        'Августа',
        'Сентября',
        'Октября',
        'Ноября',
        'Декабря'
    ];

    const monthIndex = d.getMonth();
    const monthName = months[monthIndex];
    $('.exchange-date').text(`${date}.0${monthIndex + 1}.${year}`);
    // $('.breadcrumb li').addClass('breadcrumb-item');


    const hamburger = document.querySelector('.hamburger');
    const hamburgerClose = document.querySelector('.hamburger-close');
    const menu = document.querySelector('.main-menu');
    const body = document.querySelector('body');
    hamburger.addEventListener('click', function () {
        this.classList.toggle('active');
        menu.classList.add('active');
        body.classList.add('active');
    });
    hamburgerClose.addEventListener('click', function () {
        hamburger.classList.remove('active');
        menu.classList.remove('active');
        body.classList.remove('active');
    });


    const calcModal = document.getElementById('calcModal');
    if (calcModal) {
        calcModal.addEventListener('show.bs.modal', function (event) {
            // Button that triggered the modal
            const button = event.relatedTarget;
            // Extract info from data-bs-* attributes
            const credit = button.getAttribute('data-credit');
            const month = button.getAttribute('data-month');
            const procent = button.getAttribute('data-procent');
            // If necessary, you could initiate an AJAX request here
            // and then do the updating in a callback.
            //
            // Update the modal's content.
            const modalBodyInput1 = calcModal.querySelector('.modal-body .modal-summ input');
            const modalBodyInput2 = calcModal.querySelector('.modal-body .modal-month input');
            const modalBodyInput3 = calcModal.querySelector('.modal-body .modal-procent input');
            modalBodyInput1.value = credit;
            modalBodyInput2.value = month;
            modalBodyInput3.value = procent;
        });
    }


    const searchField = document.querySelector('.search-form input');
    const searchButton = document.querySelector('.search-form button');
    if (searchField) {
        searchField.addEventListener('input', function (e) {
            const value = e.target.value;
            if (value.length > 0) {
                searchButton.style.display = 'none'
            } else {
                searchButton.style.display = 'block'
            }
        });
    }

    if (document.querySelector('#number-select')) {
        new SlimSelect({
            select: '#number-select',
            showSearch: false,
        })
    }

    if (document.querySelector('#lang-select')) {
        function selectElement(id, valueToSelect) {
            let element = document.getElementById(id);
            element.value = valueToSelect;
        }

        const srcRu = `${location.origin}/wp-content/plugins/wpglobus/flags/us.png`;
        const srcEn = `${location.origin}/wp-content/plugins/wpglobus/flags/us.png`;
        const srcKg = `${location.origin}/wp-content/plugins/wpglobus/flags/kg.png`;

        const currentLang = document.querySelector('.wpglobus-current-language img').src;
        const secondLang = document.querySelectorAll('.flag img')[1].src;
        const secondLangLink = document.querySelectorAll('.flag a')[1].href;

        if (currentLang === srcRu) selectElement('lang-select', 'ru');
        if (currentLang === srcEn) selectElement('lang-select', 'en');

        new SlimSelect({
            select: '#lang-select',
            showSearch: false,
            onChange: (info) => {
                if (currentLang !== secondLang) {
                    location.replace(secondLangLink)
                }
            }
        })
    }

    if (document.querySelectorAll('.credits-tabs a')) {
        const hash = window.location.hash;
        document.querySelectorAll('.credits-tabs a').forEach(function(el) {
            const element = el.getAttribute("href");
            if(element === hash) {
                const tab = new bootstrap.Tab(el)
                tab.show()
            }
        });
    }
    
    if (document.querySelectorAll('.online-tabs a')) {
        const hash = window.location.hash;
        document.querySelectorAll('.credits-tabs a').forEach(function(el) {
            const element = el.getAttribute("href");
            if(element === hash) {
                const tab = new bootstrap.Tab(el)
                tab.show()
            }
        });
    }
});
