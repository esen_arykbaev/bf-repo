<?php
/**
 * Template Name: Шаблон страницы компании
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
	
	<style>
		.company-news-container .news-info {
			background: rgba(41,56,69);
    		margin-top: -6px;
    		position: relative;
		}
		.news-info span {
		    font-size: 13px;
		    overflow: auto;
		    text-transform: unset;
		    line-height: 18px;
		    white-space: inherit;
		}
	</style>

<section 
class="banner" 
style="background-image: url('<?php if( get_field('fon_dlya_straniczy') ) { ?><?php the_field('fon_dlya_straniczy'); ?><?php } else { ?><?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/banner/default.jpg<?php } ?>')">
	
    <div class="container h-100">
        <div class="d-flex align-items-end justify-content-between h-100">

            <div class="banner-info">
                <h1>
                	<?php if( get_field('zagolovok_dlya_straniczy') ) { ?> 
                		<?php the_field('zagolovok_dlya_straniczy'); ?> 
                	<?php } else { ?> 
                		<?php the_title(); ?> 
                	<?php } ?>
                </h1>
                
                <?php if( get_field('opisanie_dlya_straniczy') ) { ?>
	                <p>
	                    <?php the_field('opisanie_dlya_straniczy'); ?>
	                </p>
                <?php } ?>
            </div>

            <div class="banner-menu">
            	<?php 
					$link = get_field('ssylka_1');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_2');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
				<?php 
					$link = get_field('ssylka_3');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
				    <a class="d-flex align-items-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				    	<?php echo esc_html( $link_title ); ?>
				    </a>
				<?php endif; ?>
            </div>

        </div>
    </div>
</section>


<section class="company">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xl-9">
                <div class="credits-scroll">
                    <ul class="nav nav-tabs credits-tabs" id="myTab" role="tablist">
                    	
                    	<?php if( get_field('nasha_missiya') ) { ?> 
	                		<li class="nav-item" role="presentation">
	                            <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tab-content-1" type="button" role="tab" aria-controls="tab-content-1" aria-selected="true">
	                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
								    	Our missions
							    	<?php } else { ?>
							    		Наша миссия
							    	<?php } ?>
	                            </button>
	                        </li>
	                	<?php } ?> 
	                	
                        <?php if( get_field('nasha_kompaniya') ) { ?> 
	                        <li class="nav-item" role="presentation">
	                            <button class="nav-link" id="tab-2" data-bs-toggle="tab" data-bs-target="#tab-content-2" type="button" role="tab" aria-controls="tab-content-2" aria-selected="false">
	                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
								    	Our company
							    	<?php } else { ?>
							    		Наша компания
							    	<?php } ?>
	                                
	                            </button>
	                        </li>
                        <?php } ?> 
                        
                        <?php if( get_field('nasha_proekty') ) { ?> 
	                        <li class="nav-item" role="presentation">
	                            <button class="nav-link" id="tab-3" data-bs-toggle="tab" data-bs-target="#tab-content-3" type="button" role="tab" aria-controls="tab-content-3" aria-selected="false">
	                                <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
								    	Our projects
							    	<?php } else { ?>
							    		Наши проекты
							    	<?php } ?>
	                            </button>
	                        </li>
                        <?php } ?> 
                        
                        
<!--                        <li class="nav-item" role="presentation">-->
<!--                            <button class="nav-link" id="tab-3" data-bs-toggle="tab" data-bs-target="#tab-content-3" type="button" role="tab" aria-controls="tab-content-3" aria-selected="false">-->
<!--                                Совет директоров-->
<!--                            </button>-->
<!--                        </li>-->
                        <!--<li class="nav-item" role="presentation">-->
                        <!--    <button class="nav-link" id="tab-4" data-bs-toggle="tab" data-bs-target="#tab-content-4" type="button" role="tab" aria-controls="tab-content-4" aria-selected="false">-->
                        <!--        Менеджмент-->
                        <!--    </button>-->
                        <!--</li>-->
                    </ul>
                </div>
                <div class="tab-content credits-tabs-content" id="myTabContent">
                	<?php if( get_field('nasha_missiya') ) { ?> 
	                    <div class="tab-pane fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">
	                        <?php the_field('nasha_missiya'); ?> 
	                    </div>
                    <?php } ?>
                    
                    <?php if( get_field('nasha_kompaniya') ) { ?> 
	                    <div class="tab-pane fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
	                        <?php the_field('nasha_kompaniya'); ?>
	                    </div>
                    <?php } ?>
                    
                    <?php if( get_field('nasha_proekty') ) { ?> 
	                    <div class="tab-pane fade" id="tab-content-3" role="tabpanel" aria-labelledby="tab-3">
	                        <?php the_field('nasha_proekty'); ?>
	                    </div>
                    <?php } ?>
                </div>
            </div>

            <div class="col-sm-12 col-xl-3">
            	<style>
            		.ms-slide .ms-slide-bgcont img {
						border-radius: 10px;
						}
            	</style>
            	<div class="mt-5">
            		
            	
					
					<section class="news-container company-news-container">
						<h3 class="text-center mb-3">
							<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
						    	News
					    	<?php } else { ?>
					    		Новости
					    	<?php } ?>
							
						</h3>
	                    <div class="splide" id="news-slider">
	                        <div class="splide__track">
	                            <ul class="splide__list">
	                            	<?php $params = array('posts_per_page' => 3, 'cat' => array(1) ); 
				            	    if (have_posts()) : query_posts($params); ?>
									<?php while (have_posts()) : the_post(); ?>
				                    <li class="splide__slide">
				                    	<a href="<?php the_permalink(); ?>" class="news-links text-decoration-none">
				                    		<?php if ( function_exists( 'add_theme_support' ) )
												the_post_thumbnail( array(526,9999), array('class' => 'img-fluid') ); 
											?>
		                                    <div class="news-info row">
		                                        <span class="d-inline-block text-truncate text-white" style="max-width: 348px;">
		                                           <?php the_title(); ?>
		                                        </span>
		                                        <p class="text-white mt-1 mb-0">
		                                            <?php echo get_the_date('d.m.Y'); ?>
		                                        </p>
		                                    </div>
				                    	</a>
	                                </li>
				                    <?php endwhile; ?>
									<?php endif; wp_reset_query(); ?>
	                            </ul>
	                        </div>
	                        <div class="splide__arrows d-none">
	                            <button class="splide__arrow splide__arrow--prev">
	                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
	                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
	                                </svg>
	                            </button>
	                            <button class="splide__arrow splide__arrow--next">
	                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
	                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
	                                </svg>
	                            </button>
	                        </div>
	                    </div>
	                </section>
	                <a href="/news" class="button-primary my-5" style="max-width: 288px;margin: 0 auto;">
	                	
	                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
					    	Read more
				    	<?php } else { ?>
				    		Посмотреть все новости
				    	<?php } ?>
	                </a>
					
					
					<section class="news-container company-news-container">
						<h3 class="text-center mb-3">
							<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
						    	Success stories
					    	<?php } else { ?>
					    		Истории успеха
					    	<?php } ?>
						</h3>
	                    <div class="splide" id="news-slider">
	                        <div class="splide__track">
	                            <ul class="splide__list">
	                            	<?php $params = array('posts_per_page' => 3, 'cat' => array(8) ); 
				            	    if (have_posts()) : query_posts($params); ?>
									<?php while (have_posts()) : the_post(); ?>
				                    <li class="splide__slide">
				                    	<a href="<?php the_permalink(); ?>" class="news-links text-decoration-none">
				                    		<?php if ( function_exists( 'add_theme_support' ) )
												the_post_thumbnail( array(526,9999), array('class' => 'img-fluid') ); 
											?>
		                                    <div class="news-info row">
		                                        <span class="d-inline-block text-truncate text-white" style="max-width: 348px;">
		                                           <?php the_title(); ?>
		                                        </span>
		                                        <p class="text-white mt-1 mb-0">
		                                            <?php echo get_the_date('d.m.Y'); ?>
		                                        </p>
		                                    </div>
				                    	</a>
	                                </li>
				                    <?php endwhile; ?>
									<?php endif; wp_reset_query(); ?>
	                            </ul>
	                        </div>
	                        <div class="splide__arrows d-none">
	                            <button class="splide__arrow splide__arrow--prev">
	                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
	                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
	                                </svg>
	                            </button>
	                            <button class="splide__arrow splide__arrow--next">
	                                <svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
	                                    <path d="M10.5467 1.88L8.66675 0L0.666748 8L8.66675 16L10.5467 14.12L4.44008 8L10.5467 1.88Z" fill="white"/>
	                                </svg>
	                            </button>
	                        </div>
	                    </div>
	                </section>
	                <a href="/stranicza-meropriyatii/#history" class="button-primary my-5" style="max-width: 288px;margin: 0 auto;">
	                	<?php if ( WPGlobus::Config()->language == 'en' ) { ?>
					    	Read more
				    	<?php } else { ?>
				    		Все истории успеха
				    	<?php } ?>
	                	
	                </a>
            	</div>
				

            </div>
        </div>
    </div>

</section>



<section class="partners">
    <img class="partners-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/peta-mini.png" alt="petal" />
    <div class="container">
        <?php if ( WPGlobus::Config()->language == 'en' ) { ?>
	    	<h2>Partners</h2>
    	<?php } else { ?>
    		<h2>Наши партнеры</h2>
    	<?php } ?>
        <div class="splide" id="partnersSlider">
            <div class="splide__track">
            	<ul class="splide__list">
					<?php 
					$args = array('post_type' => 'partners', 'posts_per_page' => 10  ); 
					$myposts = get_posts( $args );
					foreach( $myposts as $post ){ setup_postdata($post); ?>
				        <li class="splide__slide">
				        	<img src="<?php the_field('kartinka'); ?>" alt="">
				        </li>
					<?php } wp_reset_postdata(); ?>
				</ul>
            </div>
        </div>
    </div>
</section>

</main><!-- .site-main -->

<?php get_footer(); ?>
