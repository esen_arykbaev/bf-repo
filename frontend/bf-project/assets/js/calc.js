if ($("#slider_universal").length > 0) {
    $("#slider_universal").slider({
        range: "min",
        animate: true,
        min: 5000,
        max: 1000000,
        step: 1,
        value: 500000,
        slide: function( event, ui ) {
            $( "#slider_universal-result span" ).html( ui.value );
            $( "#universal_hidden" ).val(ui.value);
            console.log('slider ', ui.value)
        },
        change: function( event, ui ) {
            $( "#universal_hidden" ).val(ui.value);
            Calc(parseInt($('#initpay').text()))
            console.log('slider change ', ui.value)
        }
    });

    $("#slider_universal_min_payment").slider({
        range: "min",
        animate: true,
        min: 3,
        max: 60,
        step: 1,
        value: 12,
        slide: function( event, ui ) {
            // $( "#slider_universal_min_payment-result span" ).html( ui.value );
            $( "#slider_universal_min_payment-hidden" ).val(ui.value);
            console.log('slider 2 ', ui.value)
        },
        change: function( event, ui ) {
            $( "#slider_universal_min_payment-hidden" ).val(ui.value);
            $(".calc_result--btn").attr("data-month", ui.value);
            Calc(parseInt($('#initpay').text()))
            console.log('slider change 2 ', ui.value)
        }
    });

    // $("#slider_procent").slider({
    //     range: "min",
    //     animate: true,
    //     min: 23,
    //     max: 30,
    //     step: 1,
    //     value: 28,
    //     slide: function( event, ui ) {
    //         $( "#initpay" ).text(`${ui.value} %`);
    //         $( "#slider_universal_min_payment-hidden2" ).val(ui.value);
    //     },
    //     change: function( event, ui ) {
    //         $( "#slider_universal_min_payment-hidden2" ).val(ui.value);
    //         $(".calc_result--btn").attr("data-procent", ui.value);
    //         Calc(ui.value)
    //     }
    // });

    $("#slider_universal_min_payment-hidden").change(function() {
        $("#slider_universal_min_payment").slider("value", $(this).val());
    });

    $("#universal_hidden").change(function() {
        $("#slider_universal").slider("value", $(this).val());
    });

    const percentageBlock = document.querySelector('.percentage-block');
    const unitPayBlock = document.querySelector('#initpay');

    if (document.querySelector('#selectCredit')) {
        new SlimSelect({
            select: '#selectCredit',
            showSearch: false,
            onChange: (info) => {
                $('.calc_result--btn').attr("data-procent", info.value);
                unitPayBlock.innerHTML = `${info.value} %`;
                percentageBlock.value = info.value;
                Calc(info.value)
            }
        });
    }


    percentageBlock.addEventListener('change', function(){
        Calc(this.value);
        unitPayBlock.innerHTML = `${this.value} %`;
    });

    function Calc(procent = 30) {

        const price = $('#price');
        let countCredit = parseInt($('#universal_hidden').val());
        let countDay = parseInt($('#slider_universal_min_payment-hidden').val());

        let one_month_percent = procent / 100 / 12;
        let monthly = countCredit * (one_month_percent + (one_month_percent / (Math.pow((1 + one_month_percent), countDay) - 1) ));
        const allSum = monthly * countDay;
        // let es_markup1 = allSum - countCredit;
        // let es_markup_floor1 = Math.floor(es_markup1);
        const monthly_payment_floor = Math.round(monthly);
        console.log('сумма кредита', Math.round(allSum));
        console.log('ежемесячный платеж', monthly_payment_floor);

        $('.calc_result--btn').attr("data-credit", Math.round(allSum));
        $('#allSum').text(`${Math.round(allSum)} `);
        $('#allDay').text(`${Math.round(countDay)} `);
        price.text(`${monthly_payment_floor} `);

        console.log(Math.round(allSum))

        $("#myAnchor").attr("href", `/grafik-pogasheniya-kredita/?summ=${Math.round(allSum)}&srok=${Math.round(countDay)}&proc=${procent}`);

    }
    Calc();
}



