function calcCredit(t, a) {
    $(".error", a).removeClass("error");
    var r = calcdata[$('*[name="types"]', a).val()];
    null != t && !0 === t && ($(".proc", a).val(r.proc), $(".summ", a).val(r.summ), $(".srok", a).val(r.srok_do)), $("#eff_st", a).html(r.eff);
    var e = $(".proc", a),
        o = $(".summ", a),
        n = $(".srok", a);
    if (
        ((isNaN(parseInt(e.val())) || parseFloat(e.val()) < 1) && e.val(r.proc),
            isNaN(parseInt(o.val())) || parseInt(o.val()) > r.summ ? o.val(r.summ) : parseInt(o.val()) < r.min && o.val(r.min),
            isNaN(parseInt(n.val())) || 0 === parseInt(n.val()) ? n.addClass("error").attr("placeholder", calctrans.insertnumber) : parseInt(n.val()) > r.srok_do ? n.val(r.srok_do) : parseInt(n.val()) < r.srok_ot && n.val(r.srok_ot),
        0 === $(".error", a).length)
    ) {
        var s = calcData(parseInt(o.val()), parseInt(n.val()), parseFloat(e.val()));
        isNaN(s.payinmonth) || ($("#m_summ", a).html(s.payinmonth.format(0, 3, " ", "") + " " + calctrans.som), $("#i_summ", a).html(s.total.format(0, 3, " ", "") + " " + calctrans.som));
    }
    return $(".error", a).length;
}
function calcData(t, a, r) {
    var e = pmt(r, 12, a, -t, 0),
        o = [];
    o.push({ Date: new Date(), Flow: -t });
    for (var n = 1; n <= a; n++) o.push({ Date: new Date().add(n).month(), Flow: e });
    var s = XIRR(o);
    for (n = 1; n <= a; n++) (o[n].dolg = 1 === n ? t : o[n - 1].dolg - o[n - 1].pogash_osn), (o[n].pogash = (o[n].dolg * (r / 100)) / 12), (o[n].pogash_osn = e - o[n].pogash);
    return { credit: t, payinmonth: e, ef_stavka: s, total: e * a, values: o };
}
function calcRender(t, a, r) {
    var e = calcData(t, a, r),
        o = 0,
        n = 0;
    $.each(e.values, function (t, a) {
        0 < t &&
        ((o += a.pogash),
            (n += a.pogash_osn),
            $("#datatable").append(
                '<tr class="' +
                (t % 2 === 0 ? "odd" : "even") +
                '"><td>' +
                t +
                "</td><td>" +
                a.Date.toLocaleFormat("%d.%m.%y") +
                "</td><td>" +
                a.dolg.format(2, 3, " ", "") +
                "</td><td>" +
                a.pogash.format(2, 3, " ", "") +
                "</td><td>" +
                a.pogash_osn.format(2, 3, " ", "") +
                "</td><td>" +
                a.Flow.format(2, 3, " ", "") +
                "</td></tr>"
            ));
    }),
        $("#datatable").append(
            '<tr class="itogo"><td>&nbsp;</td><td>' + calctrans.itogo + ":</td><td>&nbsp;</td><td>" + o.format(2, 3, " ", "") + "</td><td>" + n.format(2, 3, " ", "") + "</td><td>" + e.total.format(2, 3, " ", "") + "</td></tr>"
        ),
        $("#emonth").html(e.payinmonth),
        $("#eplat").html(e.total.format(2, 3, " ", ""));
}
function r(t) {
    return (100 * t) / 100;
}
function pr(t, a) {
    return parseFloat(((t / 100) * a).toFixed(2));
}
/**
 * @return {string}
 */
function XIRR(t, a) {
    for (
        var r = function (t, a) {
                for (var r = a + 1, e = t[0].Flow, o = 1; o < t.length; o++) e += t[o].Flow / Math.pow(r, moment(t[o].Date).diff(moment(t[0].Date), "days") / 365);
                return e;
            },
            e = function (t, a) {
                for (var r = a + 1, e = 0, o = 1; o < t.length; o++) {
                    var n = moment(t[o].Date).diff(moment(t[0].Date), "days") / 365;
                    e -= (n * t[o].Flow) / Math.pow(r, n + 1);
                }
                return e;
            },
            o = !1,
            n = !1,
            s = 0;
        s < t.length;
        s++
    )
        0 < t[s].Flow && (o = !0), t[s].Flow < 0 && (n = !0);
    if (!o || !n) return "#NUM!";
    for (var l, p, m, d = (a = void 0 === a ? 0.1 : a), v = 0, c = !0; (l = d - (m = r(t, d)) / e(t, d)), (p = Math.abs(l - d)), (d = l), (c = 1e-10 < p && 1e-10 < Math.abs(m)) && ++v < 50; );
    return c ? "#NUM!" : d;
}
function pmt(rate, per, nper, pv, fv) {
    return (
        (fv = parseFloat(fv)),
            (nper = parseFloat(nper)),
            (pv = parseFloat(pv)),
            (per = parseFloat(per)),
            0 === per || 0 === nper
                ? 0
                : ((rate = eval(rate / (100 * per))),
                    0 === rate ? (pmt_value = -(fv + pv) / nper) : ((x = Math.pow(1 + rate, nper)), (pmt_value = (-rate * (fv + x * pv)) / (-1 + x))),
                    (pmt_value = conv_number(pmt_value, 2)),
                    parseFloat(pmt_value))
    );
}
function conv_number(expr, decplaces) {
    for (var str = "" + Math.round(eval(expr) * Math.pow(10, decplaces)); str.length <= decplaces; ) str = "0" + str;
    var decpoint = str.length - decplaces;
    return str.substring(0, decpoint) + "." + str.substring(decpoint, str.length);
}
// (Number.prototype.format = function (t, a, r, e) {
//     var o = "\\d(?=(\\d{" + (a || 3) + "})+" + (0 < t ? "\\D" : "$") + ")",
//         n = this.toFixed(Math.max(0, ~~t));
//     return (e ? n.replace(".", e) : n).replace(new RegExp(o, "g"), "$&" + (r || ","));
// }),
//     (Date.prototype.toLocaleFormat = function (t) {
//         var a = { y: this.getYear() + 1900, m: this.getMonth() + 1, d: this.getDate(), H: this.getHours(), M: this.getMinutes(), S: this.getSeconds() };
//         for (k in a) t = t.replace("%" + k, a[k] < 10 ? "0" + a[k] : a[k]);
//         return t;
//     });
