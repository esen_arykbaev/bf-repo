const gulp           = require('gulp'),
    sass           = require('gulp-sass')(require('sass')),
    cleanCSS       = require('gulp-clean-css'),
    rename         = require('gulp-rename'),
    autoprefixer   = require('gulp-autoprefixer'),
    notify         = require('gulp-notify'),
    sourcemaps     = require('gulp-sourcemaps'),
    livereload 	   = require('gulp-livereload');

gulp.task('scss', () => {
    return gulp.src('./assets/scss/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on("error", notify.onError()))
        .pipe(rename("styles.css"))
        .pipe(autoprefixer({
            cascade: false
        }))
        //.pipe(cleanCSS()) // Optional, can be disabled while debugging
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./assets/css'))
        .pipe(livereload());
});

gulp.task('watch', () => {
    livereload.listen(35729);
    gulp.watch('./assets/scss/**/*.scss', gulp.parallel('scss'));
});

gulp.task('default', gulp.parallel('scss', 'watch'));
